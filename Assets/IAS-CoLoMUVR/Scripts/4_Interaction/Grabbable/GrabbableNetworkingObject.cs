//The following code is licensed by CC BY 4.0
//at the Immersive Arts Space, Zurich University of the Arts
//By Chris Elvis Leisi - Associate Researcher at the Immersive Arts Space

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Mirror;

namespace IAS.CoLocationMUVR
{
	public class GrabbableNetworkingObject : NetworkBehaviour, IGrabbableObject
	{
        private Transform _transform;
		[Header("Grab")]
		public bool canDrag = true;

		public bool snapToCenter = false;
		
		public GrabType grabType = GrabType.Velocity;

		private Rigidbody _rigidbody;
		
		public bool isInteracting = false;
		private VrController _attachedController;
		public float velocityFactor = 2000f;
		public float rotationFactor = 60f;
		
		private bool _startetInteracting = false;
		public bool canGrabifPlayerIsDead = true;
		private bool _initialGravity = true;

		public float hoverOutlineWidth = 0.2f;

        public MeshRenderer meshRenderer;
		protected Material _mat;

        public TextMesh debugtextMesh;

        private NetworkTransformBase _networkTransformBase;

        [SyncVar (hook = nameof(OnMovingByClientChanged))]
        public bool isMovingByClient = false;
		
		public virtual void Awake()
		{
            this._transform = this.transform;
			this._rigidbody = this.gameObject.GetComponent<Rigidbody>();
            this._networkTransformBase = this.gameObject.GetComponent<NetworkTransformBase>();
            if (this._networkTransformBase != null)
                this._networkTransformBase.clientAuthority = this.isMovingByClient;
			
			if (this._rigidbody == null)
				print("no rigidbody");
			
			else
			{
				this._initialGravity = this._rigidbody.useGravity;
			}

            if (this.meshRenderer == null)
                this.meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
            if (this.meshRenderer != null)
                this._mat = this.meshRenderer.material;

            this.UpdateDebugText();
		}
		
		
		void Update()
		{
			if (isServer)
			{
				if (this.connectionToClient != null)
				{                  
                    this._startetInteracting = true;
					//If authority has changed while object is grabed on the server
					if (this.isInteracting)
						this.AttachedController().ForceEndGrab();

					//If object is grabed by a other client and gravity is on -> deactivate gravity
					if (this._rigidbody.useGravity)
						this._rigidbody.useGravity = false;

                    if (this._networkTransformBase != null && !this._networkTransformBase.clientAuthority)
                    {
                        this._networkTransformBase.clientAuthority = true;
                        this.isMovingByClient = true;
                    }
				}
                //have authority back but gravity still not initial
                else
                {
                    if (this._networkTransformBase != null && this._networkTransformBase.clientAuthority)
                    {
                        this._networkTransformBase.clientAuthority = false;
                        this.isMovingByClient = false;
                    }

                    else if (!this.isInteracting && this._rigidbody.useGravity != this._initialGravity)
                    {
                        this._rigidbody.useGravity = this._initialGravity;
                    }
                }
            }
			else if (this.hasAuthority)
			{
				//Has authority on client and is grabing but gravity is on
				if (this.isInteracting && this._rigidbody.useGravity)
				{
					this._rigidbody.useGravity = false;
				}
                //Has authority on client and is not grabing but gravity is not initial
                if (!this.isInteracting && this._rigidbody.useGravity != this._initialGravity)
                {
                    this._rigidbody.useGravity = this._initialGravity;
                }
                //Has authority but grabing is over -> check velocity to end authority
                else if (!this.isInteracting && this._rigidbody.velocity.sqrMagnitude < 0.01f || !this.isInteracting && this._rigidbody.isKinematic)
                {
                    if (GameManager.Instance != null && GameManager.Instance.localVRPlayer != null)
                    {
                        if (this._networkTransformBase != null)
                        {
                            this._networkTransformBase.CmdTeleport(this._transform.position, this._transform.rotation);
                            Debug.Log("Network Teleport");
                        }
                        GameManager.Instance.localVRPlayer.CmdRemoveObjectAuthority(netIdentity);
                    }
				}
			}
            this.UpdateDebugText();
        }

        public void UpdateDebugText()
        {
            if (this.debugtextMesh != null)
            {
                this.debugtextMesh.text = (this.isServer ? "Server: client connection" + this.connectionToClient : "Client: ") + "\nhas authority: " + this.hasAuthority + "\nClient Transform authority: " + this._networkTransformBase.clientAuthority + "\nIs moving by client: " + this.isMovingByClient;

            }
        }

        public void OnMovingByClientChanged(bool old, bool newValue)
        {
            if (this._networkTransformBase != null)
                this._networkTransformBase.clientAuthority = this.isMovingByClient;
        }

        public override void OnStartAuthority()
        {
            base.OnStartAuthority();
            this.UpdateDebugText();
        }

        public override void OnStopAuthority()
		{
			base.OnStopAuthority();
			//if authority has changed while interacting, an other player started grabing this object
			if (this.isInteracting)
			{
				this.AttachedController().ForceEndGrab();
				
				this._rigidbody.useGravity = false;
			}
            this.UpdateDebugText();
		}

		
		#region IGrabable
		public bool CanDrag()
		{
			if (!this.canDrag)
				return this.canDrag;
			else
			{
				//retunr fals if player is dead and canGrabifPlayerIsDead is false
				if (!this.canGrabifPlayerIsDead && GameManager.Instance != null && GameManager.Instance.localVRPlayer != null && GameManager.Instance.localVRPlayer.currentHealth <= 0)
					return false;
				else
					return this.canDrag;
			}
		}

		public bool SnapToCenter()
		{
			return this.snapToCenter;
		}

		public GrabType GetGrabType()
		{
			return this.grabType;
		}


		public bool StartetInteracting()
		{
			return this._startetInteracting;
		}
		
		public bool IsInteracting()
		{
			return this.isInteracting;
		}
		public void SetIsInteracting(bool b)
		{
			this.isInteracting = b;
		}
		
		public VrController AttachedController()
		{
			return this._attachedController;
		}
		
		public void SetAttachedController(VrController controller)
		{
			this._attachedController = controller;
		}
		
		public GameObject GameObject()
		{
			return this.gameObject;
		}
		
		public float VelocitiyFactor()
		{
			return this.velocityFactor;
		}
		public float RotationFactor()
		{
			return this.rotationFactor;
		}
		public void OnStartClosestObject()
		{
			if (this._mat != null && this._mat.HasProperty("_OutlineWidth"))
				this._mat.SetFloat("_OutlineWidth", this.hoverOutlineWidth);
		}
		public void OnEndClosestObject()
		{
			if (this._mat != null && this._mat.HasProperty("_OutlineWidth"))
				this._mat.SetFloat("_OutlineWidth", 0);
		}

		public void IsClosestButtonEvent(VrController controller)
		{

		}

		//invoked by vrcontroller
		public virtual void StartGrabing()
		{
			this._startetInteracting = true;

			if (!this.isServer && GameManager.Instance != null && GameManager.Instance.localVRPlayer != null && !this.hasAuthority)
				GameManager.Instance.localVRPlayer.CmdGetObjectAuthority(netIdentity);
			else if (this.isServer)
            {
                this.netIdentity.RemoveClientAuthority();
            }
			this.StartCoroutine(StarGrabWaitingTime());

            this.UpdateDebugText();
		}

		public IEnumerator StarGrabWaitingTime()
		{
			yield return new WaitForEndOfFrame();
			if (this._rigidbody != null)
				this._rigidbody.useGravity = false;
		}

		//invoked by vrcontroller
		public virtual void EndGrabing()
		{
            //if (this._rigidbody != null)
            //this._rigidbody.useGravity = this._initialGravity;

            this.UpdateDebugText();
        }


		public void ForceEndDrag()
		{
			if (this.AttachedController() != null)
			{
				this.AttachedController().ForceEndGrab();
				
			}
		}
		
		public Rigidbody GetRigidbody()
		{
			return this._rigidbody;
		}
		#endregion
	}
}
