﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IAS.CoLocationMUVR
{
	public interface IGrabbableObject
	{
		bool CanDrag();

		bool SnapToCenter();
		
		bool StartetInteracting();

		GrabType GetGrabType();
		
		Rigidbody GetRigidbody();
		
		bool IsInteracting();
		void SetIsInteracting(bool b);
		
		VrController AttachedController();
		
		void SetAttachedController(VrController controller);
		
		GameObject GameObject();

		void OnStartClosestObject();
		void OnEndClosestObject();


		float VelocitiyFactor();
		float RotationFactor();
		
		void StartGrabing();
		void EndGrabing();
		
		void ForceEndDrag();

		void IsClosestButtonEvent(VrController controller);
	}

	public enum GrabType { Velocity, Instantly, None};
}
