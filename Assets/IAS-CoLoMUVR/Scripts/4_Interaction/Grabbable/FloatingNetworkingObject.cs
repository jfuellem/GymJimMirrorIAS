using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class FloatingNetworkingObject : NetworkBehaviour
{
	private Transform _transform;
	private Rigidbody _rigidbody;

	public float forceMultiplier = 5f;
    public float forceLimiter = 1f;

	public float startVelocityForce = 0;

	
	private Vector3 _force;

	//public float sonarIntensity = 1f;

	// Start is called before the first frame update
	void Start()
    {
		this._transform = this.transform;
		this._rigidbody = this.gameObject.GetComponent<Rigidbody>();

		if (this.isServer)
		{
			this._force = Random.insideUnitSphere * this.startVelocityForce;

			//this.AddForce(force);
		}
    }

	private void FixedUpdate()
	{
		if (this.isServer)
		{
			this._transform.Translate(this._force * Time.fixedDeltaTime, Space.World);
			if (this._force.magnitude > 0)
				this._force -= this._force * Time.fixedDeltaTime;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Hand"))
		{
			IAS.CoLocationMUVR.VRPlayerManager vrPlayer = other.transform.root.GetComponent<IAS.CoLocationMUVR.VRPlayerManager>();

			if (vrPlayer != null && vrPlayer.isLocalPlayer)
			{
				IAS.CoLocationMUVR.VrController controller = other.GetComponentInParent<IAS.CoLocationMUVR.VrController>();
				Debug.Log(other.gameObject + "; " + controller);

				if (controller != null)
				{
					Vector3 velocity = controller.velocity * this.forceMultiplier * (this._rigidbody != null ? this._rigidbody.mass : 1);
					//Vector3 dirction = (this._transform.position - other.transform.position).normalized;
					this.AddForce(velocity);
				}
			}

		}
	}

	public void AddForce(Vector3 force)
	{
		if (!this.isServer)
		{
			this.CmdAddForce(force);
		}
		else
        {
            this.AddForceWithLimit(force);
            /*if (this._rigidbody != null)
			{
				//this._rigidbody.AddForce(force, ForceMode.Impulse);
				Debug.Log("Floating Object Add force; velocity: " + this._rigidbody.velocity.magnitude);

			}*/
        }
	}

	[Command(requiresAuthority = false)]
	void CmdAddForce(Vector3 force)
	{
		Debug.Log("Floating Object Add force");
        this.AddForceWithLimit(force);
        //if (this._rigidbody != null)
        //this._rigidbody.AddForce(force, ForceMode.Impulse);
    }

	void OnCollisionEnter(Collision collision)
	{
		// Start sonar ring from the contact point
		//SimpleSonarShader_SonarSender.Instance.StartSonarRing(this._transform.position, collision.impulse.magnitude * this.sonarIntensity);
	}

	void AddForceWithLimit(Vector3 force)
    {
        //Debug.Log(force.magnitude);
        if (force.magnitude > this.forceLimiter)
            force = force.normalized * this.forceLimiter;

        this._force += force;
    }
}
