using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IAS.CoLocationMUVR
{
	public class GrabbableObject : MonoBehaviour, IGrabbableObject
	{
		[Header("Grab")]
		public bool canDrag = true;

		public bool snapToCenter = false;

		public GrabType grabType = GrabType.Velocity;

		private Rigidbody _rigidbody;

		public bool isInteracting = false;
		private VrController _attachedController;
		public float velocityFactor = 2000f;
		public float rotationFactor = 60f;

		private bool _startetInteracting = false;
		public bool canGrabifPlayerIsDead = true;
		private bool _initialGravity = true;

		public float hoverOutlineWidth = 0.2f;

		protected Material _mat;

		public virtual void Awake()
		{
			this._rigidbody = this.gameObject.GetComponent(typeof(Rigidbody)) as Rigidbody;

			if (this._rigidbody == null)
				print("no rigidbody");

			else
			{
				this._initialGravity = this._rigidbody.useGravity;
			}

			MeshRenderer meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
			if (meshRenderer != null)
				this._mat = meshRenderer.material;
		}

		

		#region IGrabable
		public bool CanDrag()
		{
			if (!this.canDrag)
				return this.canDrag;
			else
			{
				//retunr fals if player is dead and canGrabifPlayerIsDead is false
				if (!this.canGrabifPlayerIsDead && GameManager.Instance != null && GameManager.Instance.localVRPlayer != null && GameManager.Instance.localVRPlayer.currentHealth <= 0)
					return false;
				else
					return this.canDrag;
			}
		}

		public bool SnapToCenter()
		{
			return this.snapToCenter;
		}

		public GrabType GetGrabType()
		{
			return this.grabType;
		}


		public bool StartetInteracting()
		{
			return this._startetInteracting;
		}

		public bool IsInteracting()
		{
			return this.isInteracting;
		}
		public void SetIsInteracting(bool b)
		{
			this.isInteracting = b;
		}

		public VrController AttachedController()
		{
			return this._attachedController;
		}

		public void SetAttachedController(VrController controller)
		{
			this._attachedController = controller;
		}

		public GameObject GameObject()
		{
			return this.gameObject;
		}

		public float VelocitiyFactor()
		{
			return this.velocityFactor;
		}
		public float RotationFactor()
		{
			return this.rotationFactor;
		}
		public void OnStartClosestObject()
		{
			if (this._mat != null && this._mat.HasProperty("_OutlineWidth"))
				this._mat.SetFloat("_OutlineWidth", this.hoverOutlineWidth);
		}
		public void OnEndClosestObject()
		{
			if (this._mat != null && this._mat.HasProperty("_OutlineWidth"))
				this._mat.SetFloat("_OutlineWidth", 0);
		}
		public void IsClosestButtonEvent(VrController controller)
		{

		}

		//invoked by vrcontroller
		public virtual void StartGrabing()
		{
			this._startetInteracting = true;
			if (this._rigidbody != null)
				this._rigidbody.useGravity = false;
		}

		//invoked by vrcontroller
		public virtual void EndGrabing()
		{
			//if (this._rigidbody != null)
			//this._rigidbody.useGravity = this._initialGravity;
		}


		public void ForceEndDrag()
		{
			if (this.AttachedController() != null)
			{
				this.AttachedController().ForceEndGrab();

			}
		}

		public Rigidbody GetRigidbody()
		{
			return this._rigidbody;
		}
		#endregion
	}
}
