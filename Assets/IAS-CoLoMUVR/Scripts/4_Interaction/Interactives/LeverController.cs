﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IAS.CoLocationMUVR;
using UnityEngine.Events;
using UnityEngine.UI;

public class LeverController : MonoBehaviour, IGrabbableObject
{

    //Controle Lever vars
    private Transform _transform;
    private Transform _controlLeverTop; // LEVERS TOP POINT

    public Vector2 xLimitRotation = new Vector2(-90f, 90f);
    public Vector2 yLimitRotation = new Vector2(-90f, 90f);

	public bool outputFromNeagtiveOneToOne = true; //else from 0 to 1
	public float xOutputValue;
    public float yOutputValue;

    public float lerpBackValue = 0f;

    [Header("Grabing")]
    public bool canDrag = true;
    VrController _attachedController = null;

    private Rigidbody _rigidbody;

    public bool isInteracting = false;

    private bool _startetInteracting = false;

	public Image xAxisFillImage;

	void Awake()
	{
		this._transform = this.transform;
	}

	// Start is called before the first frame update
	void Start()
    {
        this._transform = this.transform;
        this._rigidbody = this.gameObject.GetComponent(typeof(Rigidbody)) as Rigidbody;

        this._controlLeverTop = new GameObject().transform;
        this._controlLeverTop.parent = this._transform.parent;

        if (this._rigidbody == null)
            print("no rigidbody");
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (this.isInteracting) // CONTROL LEVER CONTROLLER 
        {
            this._controlLeverTop.position = this._attachedController.GetCenterPoint();
            /*_controlleverTopRelative = ControlLeverTop.transform.InverseTransformPoint(transform.position);
            animator.SetFloat("Blend Z", _controlleverTopRelative.z / 2);
            if (this.joystickOutput != null && animator.GetFloat("Blend Z") < 4.5f && animator.GetFloat("Blend Z") > -4.5f)
            {
                this.joystickOutput.joyPitch = animator.GetFloat("Blend Z");
            }

            animator.SetFloat("Blend X", _controlleverTopRelative.x / 2);

            if (this.joystickOutput && animator.GetFloat("Blend X") < 4.5f && animator.GetFloat("Blend X") > -4.5f)
            {
                this.joystickOutput.joyRoll = animator.GetFloat("Blend X");
            }

            if (transform.localEulerAngles.y - RotateWhenPicked <= 60 && transform.localEulerAngles.y - RotateWhenPicked >= -60)
            {
                if (this.joystickOutput != null)
                    this.joystickOutput.joyYaw = transform.localEulerAngles.y - RotateWhenPicked;
                Lever.transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y - RotateWhenPicked, 0);
            }*/

            this._transform.LookAt(this._controlLeverTop.position);
            //Debug.Log(this._transform.localRotation.eulerAngles);
            //Limit rotation
            float xValue = this._transform.localEulerAngles.x;

            if (xValue > 180f)
                xValue -= 360f;
            xValue = Mathf.Clamp(xValue, this.xLimitRotation.x, this.xLimitRotation.y);
            xOutputValue = Mathf.InverseLerp(this.xLimitRotation.x, this.xLimitRotation.y, xValue);
			if (this.outputFromNeagtiveOneToOne)
				xOutputValue = xOutputValue * 2f - 1f;

            if (xValue < 0)
                xValue += 360f;

            float yValue = this._transform.localEulerAngles.y;

            if (yValue > 180f)
                yValue -= 360f;
            yValue = Mathf.Clamp(yValue, this.yLimitRotation.x, this.yLimitRotation.y);
            yOutputValue = Mathf.InverseLerp(this.yLimitRotation.x, this.yLimitRotation.y, yValue);
			if (this.outputFromNeagtiveOneToOne)
				yOutputValue = yOutputValue * 2f - 1f;

            if (yValue < 0)
                yValue += 360f;

			if (this.xAxisFillImage != null)
				this.xAxisFillImage.fillAmount = xOutputValue;

            this._transform.localRotation = Quaternion.Euler(xValue, yValue, 0f);
        }


        if (!this.isInteracting && this.lerpBackValue > 0f)
        {
            this._transform.localRotation = Quaternion.Slerp(this._transform.localRotation, this.outputFromNeagtiveOneToOne ? Quaternion.identity : Quaternion.Euler(this.xLimitRotation.x, this.yLimitRotation.x, 0), this.lerpBackValue * Time.fixedDeltaTime);
            this.xOutputValue = Mathf.Lerp(this.xOutputValue, 0f, this.lerpBackValue * Time.fixedDeltaTime);
            this.yOutputValue = Mathf.Lerp(this.yOutputValue, 0f, this.lerpBackValue * Time.fixedDeltaTime);

			if (this.xAxisFillImage != null)
				this.xAxisFillImage.fillAmount = xOutputValue;
		}
    }

	public void SetRotationByValues(float x, float y)
	{
		x = this.outputFromNeagtiveOneToOne ? (x + 1) / 2f : x;
		y = this.outputFromNeagtiveOneToOne ? (y + 1) / 2f : y;

		this._transform.localRotation = Quaternion.Euler(Mathf.Lerp(this.xLimitRotation.x, this.xLimitRotation.y, x), Mathf.Lerp(this.yLimitRotation.x, this.yLimitRotation.y, y), 0f);

		if (this.xAxisFillImage != null)
			this.xAxisFillImage.fillAmount = x;
	}

	public Rigidbody GetRigidbody()
    {
        return this._rigidbody;
    }


	void OnDestroy()
    {
        this.ForceEndDrag();
    }
    #region IGrabable
    public bool CanDrag()
    {
        return this.canDrag;
	}

	public bool SnapToCenter()
	{
		return false;
	}

	public bool IsInteracting()
    {
        return this.isInteracting;
    }

	public GrabType GetGrabType()
	{
		return GrabType.None;
	}
	public void OnStartClosestObject()
	{

	}
	public void OnEndClosestObject()
	{

	}

    public void IsClosestButtonEvent(VrController controller)
    {

    }

    public bool StartetInteracting()
    {
        return this._startetInteracting;
    }

    public void SetIsInteracting(bool b)
    {
        this.isInteracting = b;
    }

    public VrController AttachedController()
    {
        return this._attachedController;
    }

    public void SetAttachedController(VrController controller)
    {
        this._attachedController = controller;
    }

    public GameObject GameObject()
    {
        return this.gameObject;
    }

    public float VelocitiyFactor()
    {
        return 0f;
    }
    public float RotationFactor()
    {
        return 0f;
    }

    public void StartGrabing()
    {
        this._startetInteracting = true;
    }

    public void EndGrabing()
    {

    }


    public void ForceEndDrag()
    {
        if (this.AttachedController() != null && this.isInteracting && this.canDrag)
        {
            this.AttachedController().ForceEndGrab();

        }
    }
    #endregion
}
