using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ButtonTrigger : MonoBehaviour
{
	private Transform _transform;
	private Vector3 _restPosition;
	private Vector3 _maxPressedPosition;
	public float maxYOffset = -0.5f;
	public float triggerOffset = 0.3f;

	public UnityEvent onButtonPressedEvents;
	public UnityEvent onButtonReleaseEvents;

	private bool _buttonIsPressed = false;
	private bool _isColliding = false;

	private float _enableTime;

	private float _lastInvokeTime;
	private bool _invokeClickEvents = false;

	private void Awake()
	{
		this._transform = this.transform;
		this._restPosition = this._transform.localPosition;
		this._maxPressedPosition = this._transform.localPosition;
		this._maxPressedPosition.y += this.maxYOffset;
	}

	private void OnEnable()
	{
		this._enableTime = Time.time;
	}

	private void OnDisable()
	{
		this._isColliding = false;
		this._buttonIsPressed = false;
		this._transform.localPosition = this._restPosition;
	}

	public void Update()
	{
		if (this._invokeClickEvents)
		{
			this.onButtonPressedEvents.Invoke();
			//SoundManager.Play("event:/Effects/Triggers/ButtonClick", this.gameObject);
			this._invokeClickEvents = false;
			this._lastInvokeTime = Time.time;
		}

		if (this._transform.localPosition.y <= this._maxPressedPosition.y)
		{
			this._transform.localPosition = this._maxPressedPosition;
			/*
            if (!this._buttonIsPressed)
            {
                this._buttonIsPressed = true;
                this.onButtonPressedEvents.Invoke();
            }*/

		}
		else
		{
			if (this._buttonIsPressed && !this._isColliding && this._enableTime + 0.5f < Time.time)
			{
				this._buttonIsPressed = false;
				this.onButtonReleaseEvents.Invoke();
			}
			if (this._transform.localPosition.y > this._restPosition.y)
				this._transform.localPosition = this._restPosition;
			else
			{
				this._transform.localPosition = new Vector3(this._restPosition.x, this._transform.localPosition.y, this._restPosition.z);
			}
		}
	}

	private void OnCollisionStay(Collision collision)
	{
		this._isColliding = true;
		if (this._transform.localPosition.y <= this._maxPressedPosition.y + this.triggerOffset)
		{
			if (!this._buttonIsPressed && this._lastInvokeTime + 0.5f < Time.time)
			{
				this._buttonIsPressed = true;
				this._invokeClickEvents = true;
			}
		}

	}

	private void OnCollisionExit(Collision collision)
	{
		this._isColliding = false;
	}

#if UNITY_EDITOR
	private void OnMouseDown()
	{
		this.onButtonPressedEvents.Invoke();
		//SoundManager.Play("event:/Effects/Triggers/ButtonClick", this.gameObject);
	}
#endif
}
