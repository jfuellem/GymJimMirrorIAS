﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class NetworkLeverSender : NetworkBehaviour
{
	public LeverController leverController;

	[SyncVar(hook = nameof(UpdateLeverValue))]
	private Vector2 _leverVector;

	public override void OnStartClient()
	{
		base.OnStartClient();

		//Initialize with the updated orientation
		if (this.leverController != null)
			this.leverController.SetRotationByValues(this._leverVector.x, this._leverVector.y);
	}

	public void Update()
	{
		if (this.leverController != null && this.leverController.isInteracting)
			this.UpdateLeverValueByInteraction();
	}

	//Invoke this when the SyncVar get updated
	void UpdateLeverValue(Vector2 _, Vector2 newValue)
	{
		if (this.leverController != null && !this.leverController.isInteracting)
		{
			this.leverController.xOutputValue = this._leverVector.x;
			this.leverController.yOutputValue = this._leverVector.y;
			this.leverController.SetRotationByValues(this._leverVector.x, this._leverVector.y);
		}
	}

	//Invoke this on the Instance that has the lever interaction
	void UpdateLeverValueByInteraction()
	{
		//If it is a server; update the values
		if (this.isServer)
		{
			if (this.leverController != null)
			{
				this._leverVector.x = this.leverController.xOutputValue;
				this._leverVector.y = this.leverController.yOutputValue;
			}
		}
		//uf it is a client; send the values to the server
		else
		{
			this.CmdUpdateSteeringLeverValue(new Vector2(this.leverController.xOutputValue, this.leverController.yOutputValue));
		}
	}

	//Get data from clients
	[Command(requiresAuthority = false)]
	void CmdUpdateSteeringLeverValue(Vector2 value)
	{
		if (this.leverController != null && !this.leverController.isInteracting)
		{
			this._leverVector = value;
			this.leverController.xOutputValue = this._leverVector.x;
			this.leverController.yOutputValue = this._leverVector.y;
			this.leverController.SetRotationByValues(this._leverVector.x, this._leverVector.y);
		}
	}
}
