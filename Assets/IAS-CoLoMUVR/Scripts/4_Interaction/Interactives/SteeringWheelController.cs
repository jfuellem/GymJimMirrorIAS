using System.Collections.Generic;
using UnityEngine;
using IAS.CoLocationMUVR;

public class SteeringWheelController : MonoBehaviour, IGrabbableObject
{


    private float angleStickyOffset; // ofset between wheel rotation and hand position on grab
    private float wheelLastSpeed; // wheel speed at moment of ungrab, then reduces graduelly due to INERTIA
    private float INERTIA = 0.95f; // 1-wheel never stops // 0 - wheel stops instantly
    public float MAX_ROTATION = 360; //maximal degree rotation of the wheel
    private float WHEEL_HAPTIC_FREQUENCY = 360/12; //every wheel whill click 12 times per wheel rotation

    [Header("Steering Wheel Relative Point")]
    public GameObject WheelBase;

    [Header("Wheel & Hand relative position")]
    public Vector3 RelativePos;

    [Header("Output steering wheel angle")]
    public float outputAngle=0;
    public TextMesh textDisplay;

    [Header("Arrays Values (Debug)")]
    public List<float> lastValues = new List<float>(); // stores last angles
    public List<float> Diffs = new List<float>(); // stores difference between each last angles
    public List<float> formulaDiffs = new List<float>(); // stores formulated diffs
    public List<float> increment = new List<float>(); // calculating incrementation


    [Header("Grabing")]
    public bool canDrag = true;
    VrController _attachedController = null;

    private Rigidbody _rigidbody;

    public bool isInteracting = false;

    private bool _startetInteracting = false;

    void Start()
    {
        CreateArrays(5); // CALLING FUNCTION WHICH CREATES ARRAYS
        angleStickyOffset = 0f;
        wheelLastSpeed = 0;

        this._rigidbody = this.gameObject.GetComponent(typeof(Rigidbody)) as Rigidbody;

        if (this._rigidbody == null)
            print("no rigidbody");
    }

    void CalculateOffset()
    {
        float rawAngle = CalculateRawAngle();
        angleStickyOffset = outputAngle - rawAngle;
    }

    float CalculateRawAngle()
    {
        RelativePos = WheelBase.transform.InverseTransformPoint(_attachedController.transform.position); // GETTING RELATIVE POSITION BETWEEN STEERING WHEEL BASE AND HAND
        
        return Mathf.Atan2(RelativePos.y, RelativePos.x) * Mathf.Rad2Deg; // GETTING CIRCULAR DATA FROM X & Y RELATIVES  VECTORS
    }

    void FixedUpdate()
    {
        float angle;
        if (this.isInteracting)
        {
            angle = CalculateRawAngle() + angleStickyOffset; // When hands are holding the wheel hand dictates how the wheel moves
															 // angleSticky Offset is calculated on wheel grab - makes wheel not to rotate instantly to the users hand
		}
        else
        {
            // when wheel is released we apply a little of inertia
            angle = outputAngle + wheelLastSpeed; //last wheel speed is updated when wheel is ungrabbed and then gradually returns to zero
            wheelLastSpeed *= INERTIA;
        }
        lastValues.RemoveAt(0); // REMOVING FIRST ITEM FROM ARRAY
        lastValues.Add(angle); // ADD LAST ITEM TO ARRAY

        outputAngle = hookedAngles(angle);// SETTING OUTPUT THROUGH FUNCTION

		if (this.isInteracting)
			wheelLastSpeed = outputAngle - lastValues[3];

		if (textDisplay != null){
            textDisplay.text = Mathf.Round(outputAngle) + "" + ".00 deg. speed " + wheelLastSpeed;
        }
        transform.localEulerAngles = new Vector3(outputAngle+90, -90, -90);// ROTATE WHEEL MODEL FACING TO THE PLAYA

        float haptic_speed_coeff = Mathf.Abs(lastValues[4] - lastValues[3]) + 1;
        if (Mathf.Abs(outputAngle % WHEEL_HAPTIC_FREQUENCY) <= haptic_speed_coeff &&
            Mathf.Abs(lastValues[3] % WHEEL_HAPTIC_FREQUENCY) > haptic_speed_coeff)
        {
            if (_attachedController != null)
            {
				_attachedController.SetVribration(0.5f, 0.5f, 0.2f);
                //TrackedController.TriggerHapticPulse(1000);
            }
        }
    }


    void CreateArrays(int firstPparam) // FUNCTION WHICH CREATING ARRAYS
    {
        for (int i = 0; i < firstPparam; i++) // FOR LOOP WITH PARAM
        {
            lastValues.Add(0);  // LAST VALUES ARRAY
        }


        for (int i = 0; i < firstPparam - 1; i++) // FOR LOOP WITH PARAM -1
        {
            Diffs.Add(0); // ARRAY TO STORE DIFFERECE BETWEEN NEXT AND PREV
            formulaDiffs.Add(0); // ARRAY TO STORE FORMULATED DIFFS
            increment.Add(0); //  ARRAY TO STORE INCREMENT FOR EACH WHEEL SPIN
        }
    }


    public float hookedAngles(float angle) // FORMULATING AND CALCULATING FUNCTION WHICH COUNTS SPINS OF WHEEL
        //Also applying rotation limits
    {
        float period = 360;
        for (int i = 0; i < lastValues.Count - 1; i++)
        {
            Diffs.RemoveAt(0);
            Diffs.Add(lastValues[i + 1] - lastValues[i]);
        }

        for (int i = 0; i < formulaDiffs.Count; i++)
        {
            formulaDiffs.RemoveAt(0);
            var a = (Diffs[i] + period / 2.0f);
            var b = period;
            var fdiff = a - Mathf.Floor(a / b) * b;
            formulaDiffs.Add(fdiff - period / 2);
        }

        for (int i = 0; i < formulaDiffs.Count; i++)
        {
            increment.RemoveAt(0);
            increment.Add(formulaDiffs[i] - Diffs[i]);
        }

        for (int i = 1; i < formulaDiffs.Count; i++)
        {
            increment[i] += increment[i - 1];
        }

        lastValues[4] += increment[3];

		/*
        if (Mathf.Abs(lastValues[4]) > MAX_ROTATION)
        {
            lastValues[4] = lastValues[3];
            if (_attachedController!=null)
            {
                //TrackedController.TriggerHapticPulse(500);
            }
        }*/
        

        return lastValues[4]; // COLLIBRATE TO ZERO WHEN STILL AND RETURN CALCULATED VALUE
    }

	public float GetSpeed()
	{
		return this.wheelLastSpeed;
	}

	public Rigidbody GetRigidbody()
    {
        return this._rigidbody;
    }

    void OnDestroy()
    {
        this.ForceEndDrag();
    }
    #region IGrabable
    public bool CanDrag()
    {
        return this.canDrag;
    }

	public bool SnapToCenter()
	{
		return false;
	}

	public GrabType GetGrabType()
	{
		return GrabType.None;
	}
	public void OnStartClosestObject()
	{

	}
	public void OnEndClosestObject()
	{

	}

    public void IsClosestButtonEvent(VrController controller)
    {

    }

    public bool IsInteracting()
    {
        return this.isInteracting;
    }


    public bool StartetInteracting()
    {
        return this._startetInteracting;
    }

    public void SetIsInteracting(bool b)
    {
        this.isInteracting = b;
    }

    public VrController AttachedController()
    {
        return this._attachedController;
    }

    public void SetAttachedController(VrController controller)
    {
        this._attachedController = controller;
    }

    public GameObject GameObject()
    {
        return this.gameObject;
    }

    public float VelocitiyFactor()
    {
        return 0f;
    }
    public float RotationFactor()
    {
        return 0f;
    }

    public void StartGrabing()
    {
        this._startetInteracting = true;
        this.CalculateOffset();
        //this.OnStick(this._attachedController);
    }

    public void EndGrabing()
    {
        wheelLastSpeed = outputAngle - lastValues[3];

        //this.OnUnStick();
    }


    public void ForceEndDrag()
    {
        if (this.AttachedController() != null && this.isInteracting && this.canDrag)
        {
            this.AttachedController().ForceEndGrab();

        }
    }
    #endregion

}
