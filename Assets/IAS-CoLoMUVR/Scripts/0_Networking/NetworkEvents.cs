﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Mirror;

public class NetworkEvents : NetworkBehaviour
{
	public UnityEvent onStartServerEvents; //Invoke this if the object starts on a server
	public UnityEvent onStartClientEvents; //Invoke this if the object starts on a client

	public UnityEvent networkEvents;


	// Start is called before the first frame update
	void Start()
    {
        if (NetworkManager.singleton != null)
		{
			if (NetworkManager.singleton.mode == NetworkManagerMode.ServerOnly)
				this.onStartServerEvents.Invoke();
			else if (NetworkManager.singleton.mode == NetworkManagerMode.ClientOnly)
				this.onStartClientEvents.Invoke();
			//If the object starts on a Host, invoke both
			else if (NetworkManager.singleton.mode == NetworkManagerMode.Host)
			{
				this.onStartServerEvents.Invoke();
				this.onStartClientEvents.Invoke();
			}
		}
	}

	//Invoke this on the local instanc
    public void TriggerEvent()
	{
		//if it is a server it invokes the events
		if (this.isServer)
			this.networkEvents.Invoke();
		//if not, it sends a command to the server
		else
			this.CmdTriggerNetworkEvents();
	}

	//Invoke this on the server instance
	[Command(requiresAuthority = false)]
	void CmdTriggerNetworkEvents()
	{
		this.networkEvents.Invoke();
	}
}
