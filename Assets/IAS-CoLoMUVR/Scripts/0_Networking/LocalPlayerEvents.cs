﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.Events;

public class LocalPlayerEvents : NetworkBehaviour
{

	public UnityEvent onStartLocalPlayerEvents;

	public override void OnStartLocalPlayer()
	{
		base.OnStartLocalPlayer();
		this.onStartLocalPlayerEvents.Invoke();
	}
}
