﻿//The following code is licensed by CC BY 4.0
//at the Immersive Arts Space, Zurich University of the Arts
//By Chris Elvis Leisi - Associate Researcher at the Immersive Arts Space

using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Mirror;

namespace IAS.CoLocationMUVR {
	//custom message used for spawning different player prefabs
	public struct PlayerSpawnMessage : NetworkMessage
	{
		public bool isVrPlayer; //if false: spawn a camera player //The camera player prefab is in the base mirror playerPrefab slot
		public int index;

		public void Deserialize(NetworkReader reader) { }

		public void Serialize(NetworkWriter writer) { }
	}

	[AddComponentMenu("")]
	public class IANetworkManager : NetworkManager
	{
		//let you use different VRPlayers
		public List<GameObject> vrPlayersPrefabs = new List<GameObject>();

		//register the SpawnMessage
		public override void OnStartServer()
		{
			base.OnStartServer();
			NetworkServer.RegisterHandler<PlayerSpawnMessage>(OnCreateCharacter);
		}

		//this function get invoked when a PlayerSpawnMessage is received
		void OnCreateCharacter(NetworkConnectionToClient conn, PlayerSpawnMessage message)
		{
			GameObject prefab = this.GetCorrectPlayerPrefab(message.isVrPlayer, message.index);
			if (prefab != null)
			{
				GameObject gameObject = Instantiate(prefab);
				//call this to use this gameobject as the primary player
				NetworkServer.AddPlayerForConnection(conn, gameObject);

				//add serverUI info text if it exists
				if (ServerUI.Instance != null)
				{
					if (message.isVrPlayer)
						ServerUI.Instance.AddServerLogText("New Player has joined the Server " + conn.address +  " : Type index " + message.index);
					else
						ServerUI.Instance.AddServerLogText("New Player has joined the Server " + conn.address +  " : Camera");

					ServerUI.Instance.AddServerLogText("-Current connections: " + NetworkServer.connections.Count);
				}
			}
		}

		public override void OnServerDisconnect(NetworkConnectionToClient conn)
		{
			//add serverUI info text if it exists
			if (ServerUI.Instance != null)
			{
				ServerUI.Instance.AddServerLogText("Player disconnect");
				ServerUI.Instance.AddServerLogText("-Current connections: " + NetworkServer.connections.Count);
			}
			base.OnServerDisconnect(conn);
		}

		public override void OnClientConnect()
		{
			base.OnClientConnect();

			StartCoroutine(WaitForGameManager());
			//conn.Send(characterMessage);
		}

		private IEnumerator WaitForGameManager()
		{
			float t = 0;
			while (t < 5f)
			{
				yield return new WaitForEndOfFrame();
				t += Time.deltaTime;

				if (PlayerIndiciesManager.Instance != null)
				{
					Debug.Log("Found indicies manager " + t);
					break;
				}
			}

			//send the PlayerSpawnMessage when the client connect
			PlayerSpawnMessage characterMessage = new PlayerSpawnMessage
			{
				//take data from static variables
				isVrPlayer = GlobalVariables.joinAsVRPlayer,
				index = GlobalVariables.playerTypeIndex
			};
			Debug.Log("Send client connect to server");

			NetworkClient.connection.Send(characterMessage);
		}

		/*
		public override void OnClientConnect(NetworkConnectionToClient conn)
		{
			base.OnClientConnect(conn);


		}*/

		//get the right player prefab to spawn
		GameObject GetCorrectPlayerPrefab(bool joinsAsVRPlayer, int index)
		{
			if (joinsAsVRPlayer)
			{
				if (index >= 0 && this.vrPlayersPrefabs.Count > index)
					return this.vrPlayersPrefabs[index];
				else
					return this.playerPrefab;
			}
			else
				return this.playerPrefab;
		}

		//register the vrPlayer prefabs
		public override void RegisterClientMessages()
		{
			base.RegisterClientMessages(); for (int i = 0; i < vrPlayersPrefabs.Count; i++)
			{
				GameObject prefab = vrPlayersPrefabs[i];
				if (prefab != null)
				{
					NetworkClient.RegisterPrefab(prefab);
				}
			}
		}
	} 
}
