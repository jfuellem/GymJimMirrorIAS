//The following code is licensed by CC BY 4.0
//at the Immersive Arts Space, Zurich University of the Arts
//By Chris Elvis Leisi - Associate Researcher at the Immersive Arts Space

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace IAS.CoLocationMUVR
{
	public class GameManager : MonoBehaviour
	{
		public static GameManager Instance;
		//get the local player of the client/host
		public VRPlayerManager localVRPlayer;
		public bool gameIsRunning;
		public Transform calculationTarget;

		//scene specific offset of the AR camera calibration
		public Vector3 arCameraCalibrationOffset = Vector3.zero;

		//own guardian mesh
		public GameObject roomGuardianObject;

		
		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			//turn of the guardian mesh if you are not a VR player
			if (!GlobalVariables.joinAsVRPlayer)
				this.DeactivateGuardianWall();

		}

		public void DeactivateGuardianWall()
		{
			if (this.roomGuardianObject != null)
				this.roomGuardianObject.SetActive(false);
		}


		//invoke to calibrate the center of the space
		public void CalculateCenterOffset(Vector3 offset, bool replaceObjects)
		{
			if (this.localVRPlayer != null)
				this.CalibrateCenterWithPlayer(this.localVRPlayer.center, this.localVRPlayer.leftHand.transform, this.localVRPlayer.rightHand.transform, offset, false);
			else
			{
				MenuPlayerManager menuPlayer = (MenuPlayerManager)GameObject.FindObjectOfType(typeof(MenuPlayerManager));
				if (menuPlayer != null)
					this.CalibrateCenterWithPlayer(menuPlayer.center, menuPlayer.leftHand.transform, menuPlayer.rightHand.transform, offset, replaceObjects);
			}

		}

		void CalibrateCenterWithPlayer(Transform center, Transform leftHand, Transform rightHand, Vector3 offset, bool replaceObjects)
		{
			Debug.Log(center.gameObject + "; " + leftHand.gameObject + "; " + rightHand.gameObject + "; " + offset + "; " + replaceObjects);
			Vector3 yNeutralPos;
			if (replaceObjects)
			{
				if (this.calculationTarget == null)
				{
					this.calculationTarget = new GameObject().transform;
					this.calculationTarget.parent = center.parent;
				}

				this.calculationTarget.position = rightHand.position;
				yNeutralPos = leftHand.position;
				yNeutralPos.y = rightHand.position.y;
				this.calculationTarget.transform.LookAt(yNeutralPos, center.up);

				//after calculate rotation set to right hand position
				this.calculationTarget.position = rightHand.position;

				Vector3 posOffset = this.calculationTarget.position * -1f;
				//Replace objects
				//this.setNewCenterPoint = false;
				Debug.Log("replace Objects");
			}


			center.localPosition = Vector3.zero;
			center.localRotation = Quaternion.identity;

			if (this.calculationTarget == null)
			{
				this.calculationTarget = new GameObject().transform;
				this.calculationTarget.parent = center.parent;
			}

			//set rotation
			this.calculationTarget.position = rightHand.transform.position;
			yNeutralPos = leftHand.transform.position;
			yNeutralPos.y = rightHand.transform.position.y;
			this.calculationTarget.transform.LookAt(yNeutralPos, center.up);

			//after calculate rotation set to right hand position
			this.calculationTarget.position = rightHand.transform.position;

			//set offset
			center.localPosition = this.calculationTarget.InverseTransformPoint(Vector3.zero) + offset;
			center.localRotation = Quaternion.Inverse(this.calculationTarget.rotation);

			GlobalVariables.centerPositionOffset = center.localPosition;
			GlobalVariables.centerRotationOffset = center.localRotation;
			Debug.Log("Calibrate -center");

		}
	}
}
