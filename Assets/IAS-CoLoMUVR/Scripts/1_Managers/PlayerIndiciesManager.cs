using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IAS.CoLocationMUVR
{
	public class PlayerIndiciesManager : MonoBehaviour
	{
		public static PlayerIndiciesManager Instance;
		public int[] possiblePlayerIndices;

		private void Awake()
		{
			Instance = this;
			if (this.possiblePlayerIndices != null && this.possiblePlayerIndices.Length > 0)
				this.SetPlayerSpawnIndex();
			else
				GlobalVariables.playerTypeIndex = 0;
		}

		public void SetPlayerSpawnIndex()
		{
			if (!this.ContainsPlayerIndex(GlobalVariables.playerTypeIndex, out int firstIndex))
			{
				GlobalVariables.playerTypeIndex = firstIndex;
			}
		}

		public bool ContainsPlayerIndex(int id, out int firstID)
		{
			firstID = 0;
			if (this.possiblePlayerIndices.Length > 0)
			{
				Debug.Log("has player id: " + id + "; first index = " + this.possiblePlayerIndices[0]);

				firstID = this.possiblePlayerIndices[0];
				for (int i = 0; i < this.possiblePlayerIndices.Length; i++)
				{
					if (this.possiblePlayerIndices[i] == id)
						return true;
				}
			}
			return false;
		}
	}
}