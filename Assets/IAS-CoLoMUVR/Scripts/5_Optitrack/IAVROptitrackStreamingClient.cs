using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IAS.CoLocationMUVR;

public class IAVROptitrackStreamingClient : OptitrackStreamingClient
{
	public override void OnEnable()
	{
		Debug.Log("Set Optitrack Server Adress:" + GlobalVariables.optitrackServerAddress + "; Local Adress: " + GlobalVariables.optitrackLocalAddress);
		if (GlobalVariables.optitrackServerAddress.Length > 0)
			this.ServerAddress = GlobalVariables.optitrackServerAddress;
		if (GlobalVariables.optitrackLocalAddress.Length > 0)
			this.LocalAddress = GlobalVariables.optitrackLocalAddress;
		base.OnEnable();
	}

	public override void Update()
	{
		base.Update();

		if (Input.GetKeyDown(KeyCode.R))
		{
			Debug.Log("try restart optitrack");
			this.OnDisable();
			base.OnEnable();
		}
	}
}
