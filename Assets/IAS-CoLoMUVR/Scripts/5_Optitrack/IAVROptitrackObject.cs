using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


namespace IAS.CoLocationMUVR.Optitrack
{
    public class IAVROptitrackObject : NetworkBehaviour
    {
        protected IAVROptitrackNetworkClient _streamingClient;

        public virtual void Start()
        {
            this._streamingClient = FindObjectOfType<IAVROptitrackNetworkClient>();
            if (this._streamingClient != null)
                this._streamingClient.Add(this);
        }

        public virtual void OnDestroy()
        {

            if (this._streamingClient != null)
            {
                this._streamingClient.Remove(this);
            }
        }

        public virtual void Initialize(bool addOptitrackComponent)
        {
            Debug.Log("Initialize Optitrack Object " + this.gameObject.name);
        }
    }
}