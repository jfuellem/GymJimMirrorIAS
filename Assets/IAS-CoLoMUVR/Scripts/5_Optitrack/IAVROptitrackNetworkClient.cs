using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


namespace IAS.CoLocationMUVR.Optitrack
{
    public class IAVROptitrackNetworkClient : NetworkBehaviour
    {
        public bool activateOptitrackOnWindows = true;
        public bool activateOptitrackOnMacOS = true;

        public GameObject streamingClientPrefab;

        private OptitrackStreamingClient _optitrackStreamingClient;

        public List<IAVROptitrackObject> optitrackObjects;

        private bool _isInitialized = false;

        public override void OnStartServer()
        {
            base.OnStartServer();

            if (this.isServerOnly && GlobalVariables.startOptitrackConnection)
            {
                this.CreateMotiveClient();
            }
        }

        public override void OnStartClient()
        {
            base.OnStartClient();

            if (GlobalVariables.startOptitrackConnection)
            {
                //Runnns on a Windows or MacOS system
                this.CreateMotiveClient();
            }
        }

        void CreateMotiveClient()
        {
            if (this.streamingClientPrefab != null)
            {
                GameObject obj = Instantiate(this.streamingClientPrefab, this.transform);
                this._optitrackStreamingClient = obj.GetComponent<OptitrackStreamingClient>();
                if (this._optitrackStreamingClient != null)
                    StartCoroutine(this.WaitStartTime());
            }
            
        }

        public IEnumerator WaitStartTime()
        {
            yield return new WaitForSeconds(0.2f);

            for (int i = 0; i < this.optitrackObjects.Count; i++)
                this.optitrackObjects[i].Initialize(this._optitrackStreamingClient != null? true : false);

            this._isInitialized = true;
        }

        public void Add(IAVROptitrackObject target)
        {
            if (this.optitrackObjects == null)
                this.optitrackObjects = new List<IAVROptitrackObject>();

            if (!this.optitrackObjects.Contains(target))
            {
                this.optitrackObjects.Add(target);
                if (this._isInitialized)
                    target.Initialize(this._optitrackStreamingClient != null ? true : false);
            }
        }

        public void Remove(IAVROptitrackObject target)
        {
            if (this.optitrackObjects != null && this.optitrackObjects.Contains(target))
                this.optitrackObjects.Remove(target);
        }
    }
}
