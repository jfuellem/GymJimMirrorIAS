using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace IAS.CoLocationMUVR.Optitrack
{
    public class IAVROptitrackSkeleton : IAVROptitrackObject
    {
        public string skeletonName;

        public GameObject armatureParent;

        public Avatar destinationAvatar;

        private OptitrackSkeletonAnimator _optitrackSkeleton;


        private Animator _animator;


        [Range(0f, 1f)] public float lookAtWeight = 1f;
        [Range(0f, 1f)] public float goalWeight = 0.5f;
        [Range(0f, 1f)] public float hintWeight = 0.25f;

        public Vector3 headLookAtPoint = Vector3.left;

        public Transform headBone;

		[SyncVar(hook = nameof(OnChangeBodycenterPositionOffset))]
		private Vector3 _bodyCenterPositionOffset;

		[SyncVar(hook = nameof(OnChangeBodycenterRotationOffset))]
		private Vector3 _bodyCenterRotationOffset;

		[Header("IKs")]
        public Transform head;
        public Transform bodyCenterIK;
		public Transform leftHand;
        public Transform leftElbow;
        public Transform rightHand;
        public Transform rightElbow;
        public Transform leftFoot;
        public Transform leftKnee;
        public Transform rightFoot;
        public Transform rightKnee;

        public bool debugCopier = false;

		[System.Serializable]
        public class LinkedTransformObject
        {
            public Transform target;
            public Transform goal;
        }

        public LinkedTransformObject[] linkedTransformObjects;

        public override void Initialize(bool addOptitrackComponent)
        {
            base.Initialize(addOptitrackComponent);
            if (addOptitrackComponent)
            {
                this._optitrackSkeleton = this.armatureParent.AddComponent<OptitrackSkeletonAnimator>();
                if (this._optitrackSkeleton != null)
                {
                    this._optitrackSkeleton.SkeletonAssetName = this.skeletonName;
                    this._optitrackSkeleton.DestinationAvatar = this.destinationAvatar;

					if (this._animator == null)
						this._animator = this.GetComponentInChildren<Animator>();
					if (this._animator)
					{
						if (this.bodyCenterIK != null)
						{
							this.bodyCenterIK.parent.position = this.linkedTransformObjects[0].goal.position;
							this.bodyCenterIK.parent.rotation = this.linkedTransformObjects[0].goal.rotation;

							this.bodyCenterIK.position = this._animator.bodyPosition;
							this.bodyCenterIK.rotation = this._animator.bodyRotation;

							if (this.isServer)
							{
								this._bodyCenterPositionOffset = this.bodyCenterIK.localPosition;
								this._bodyCenterRotationOffset = this.bodyCenterIK.localEulerAngles;
							}
						}
						this._animator.enabled = false;
					}
				}
                if (!this.isServer)
                    this.TurnOffSyncOnClients();
            }
            else
            {
                //this.armatureParent.GetComponent<RootMotion.FinalIK.VRIK>().enabled = true;
            }
        }


		void OnChangeBodycenterPositionOffset(Vector3 _, Vector3 newVector)
		{
			if (this.bodyCenterIK)
				this.bodyCenterIK.localPosition = newVector;
		}

		void OnChangeBodycenterRotationOffset(Vector3 _, Vector3 newVector)
		{
			if (this.bodyCenterIK)
				this.bodyCenterIK.localEulerAngles = newVector;
		}


		void TurnOffSyncOnClients()
        {
            NetworkTransform networkTransform = this.gameObject.GetComponent<NetworkTransform>();

            if (networkTransform != null)
            {
                networkTransform.syncPosition = false;
                networkTransform.syncRotation = false;
                networkTransform.syncScale = false;
            }

            NetworkTransformChild[] networkTransformChilds = this.gameObject.GetComponents<NetworkTransformChild>();

            if (networkTransformChilds != null)
            {
                foreach(NetworkTransformChild networkTransformChild in networkTransformChilds)
                {
                    networkTransformChild.syncPosition = false;
                    networkTransformChild.syncRotation = false;
                    networkTransformChild.syncScale = false;
                }
            }
        }

        /*
        public override void OnStartClient()
        {
            if (this.isClientOnly)
                this.armatureParent.GetComponent<RootMotion.FinalIK.VRIK>().enabled = true;
        }*/

        public void LateUpdate()
        {
			/*if (this._optitrackSkeleton != null)
            {
                for (int i = 0; i < this.linkedTransformObjects.Length; i++)
                {
                    this.linkedTransformObjects[i].goal.position = this.linkedTransformObjects[i].target.position;
                    this.linkedTransformObjects[i].goal.rotation = this.linkedTransformObjects[i].target.rotation;
                }
            }*/

			if (this._optitrackSkeleton != null && this.isServer || this.debugCopier)
			{
				for (int i = 0; i < this.linkedTransformObjects.Length; i++)
				{
					this.linkedTransformObjects[i].target.position = this.linkedTransformObjects[i].goal.position;
					this.linkedTransformObjects[i].target.rotation = this.linkedTransformObjects[i].goal.rotation;
				}
			}
        }

        public void BodyIK()
        {
            if (this._animator == null)
                this._animator = this.GetComponentInChildren<Animator>();

            Debug.Log("On Animator IK");
            if (this.debugCopier  || this._optitrackSkeleton == null)
			{
				// _animator.GetBoneTransform(HumanBodyBones.Hips).position = centerHip;
				_animator.bodyPosition = this.bodyCenterIK.position;
				_animator.bodyRotation = this.bodyCenterIK.rotation;

				//this.linkedTransformObjects[0].goal.position = this.linkedTransformObjects[0].target.position;
				//this.linkedTransformObjects[0].goal.rotation = this.linkedTransformObjects[0].target.rotation;

				_animator.SetIKHintPositionWeight(AvatarIKHint.RightElbow, hintWeight);
                _animator.SetIKHintPosition(AvatarIKHint.RightElbow, this.rightElbow.position);

                _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, goalWeight);
                _animator.SetIKPosition(AvatarIKGoal.RightHand, this.rightHand.position);
                _animator.SetIKRotation(AvatarIKGoal.RightHand, this.rightHand.rotation);

                _animator.SetIKHintPositionWeight(AvatarIKHint.LeftElbow, hintWeight);
                _animator.SetIKHintPosition(AvatarIKHint.LeftElbow, this.leftElbow.position);

                _animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, goalWeight);
                _animator.SetIKPosition(AvatarIKGoal.LeftHand, this.leftHand.position);
                _animator.SetIKRotation(AvatarIKGoal.LeftHand, this.leftHand.rotation);


                _animator.SetIKHintPositionWeight(AvatarIKHint.RightKnee, hintWeight);
                _animator.SetIKHintPosition(AvatarIKHint.RightKnee, this.rightKnee.position);

                _animator.SetIKPositionWeight(AvatarIKGoal.RightFoot, goalWeight);
                _animator.SetIKPosition(AvatarIKGoal.RightFoot, this.rightFoot.position);
                _animator.SetIKRotation(AvatarIKGoal.RightFoot, this.rightFoot.rotation);

                _animator.SetIKHintPositionWeight(AvatarIKHint.LeftKnee, hintWeight);
                _animator.SetIKHintPosition(AvatarIKHint.LeftKnee, this.leftKnee.position);

                _animator.SetIKPositionWeight(AvatarIKGoal.LeftFoot, goalWeight);
                _animator.SetIKPosition(AvatarIKGoal.LeftFoot, this.leftFoot.position);
                _animator.SetIKRotation(AvatarIKGoal.LeftFoot, this.leftFoot.rotation);

                //this.headBone.position = this.head.position;
                //this.headBone.rotation = this.head.rotation;

                _animator.SetLookAtWeight(lookAtWeight);
                Vector3 lookAtPoint = this.head.TransformPoint(this.headLookAtPoint);
                _animator.SetLookAtPosition(lookAtPoint);
                Debug.DrawLine(this.head.position, lookAtPoint, Color.blue);
            }
			else
			{
                this.bodyCenterIK.position = _animator.bodyPosition;
                this.bodyCenterIK.rotation = _animator.bodyRotation;

                this.head.position = this.headBone.position;
                this.head.rotation = this.headBone.rotation;

                this.rightHand.position = _animator.GetIKPosition(AvatarIKGoal.RightHand);
                this.rightHand.rotation = _animator.GetIKRotation(AvatarIKGoal.RightHand);
                this.rightElbow.position = _animator.GetIKHintPosition(AvatarIKHint.RightElbow);

                this.leftHand.position = _animator.GetIKPosition(AvatarIKGoal.LeftHand);
                this.leftHand.rotation = _animator.GetIKRotation(AvatarIKGoal.LeftHand);
                this.leftElbow.position = _animator.GetIKHintPosition(AvatarIKHint.LeftElbow);

                this.rightFoot.position = _animator.GetIKPosition(AvatarIKGoal.RightFoot);
                this.rightFoot.rotation = _animator.GetIKRotation(AvatarIKGoal.RightFoot);
                this.rightKnee.position = _animator.GetIKHintPosition(AvatarIKHint.RightKnee);

                this.leftFoot.position = _animator.GetIKPosition(AvatarIKGoal.LeftFoot);
                this.leftFoot.rotation = _animator.GetIKRotation(AvatarIKGoal.LeftFoot);
                this.leftKnee.position = _animator.GetIKHintPosition(AvatarIKHint.LeftKnee);
            }
        }
    }
}
