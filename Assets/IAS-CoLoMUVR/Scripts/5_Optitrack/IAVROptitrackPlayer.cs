using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace IAS.CoLocationMUVR.Optitrack
{
    public class IAVROptitrackPlayer : IAVROptitrackObject
    {
        [SyncVar(hook = nameof(OnUpdateLocalCalubration))]
        public bool doLocalCalibration = false;
        private bool _initialized = false;

        private VRPlayerManager _vRPlayerManager;

        private OptitrackRigidBody _optitrackRigidBody;

        private Transform _optitrackRigidbodyTransform;
        private Transform _calibrationHead;
        private Transform _calibrationCenter;

        public float offsetCheckInterval = 0.2f;
        private float _lastOffsetCheckTime;
        public int offsetRecordeLength = 10;
        private Vector3[] _lastOffsets;
        private int _offsetPointer = -1;


        public Vector2 offsetTriggerValue = new Vector2(0.012f, 0.30f);
        public Vector2 offsetVelocity = new Vector2(0.08f, 0.15f);
        public float calibrationTriggerValue = 0.01f;

        private bool _offsetRecognized = false;
        private bool _isCalibrating = false;

        private float _calibrationStartTime;
        public float calibrationWaitTime = 5f;

        public float recordPositionsTime = 2f;
        public int recordPositionsLength = 15;


        private Vector3 _lastPosition;
        private Vector3[] _lastPositions;
        private int _lastPositionPointer = -1;
        private float _nextPositionRecordeTime;

        public override void Initialize(bool addOptitrackComponent)
        {
            base.Initialize(addOptitrackComponent);
            if (addOptitrackComponent && this.isLocalPlayer && GlobalVariables.useOptitrackCalibration || addOptitrackComponent && !this.isLocalPlayer && !this.doLocalCalibration)
            {
                this._initialized = true;
                this._vRPlayerManager = this.gameObject.GetComponent<VRPlayerManager>();

                if (this._vRPlayerManager != null)
                {
                    this._optitrackRigidbodyTransform = new GameObject().transform;
                    this._optitrackRigidbodyTransform.gameObject.name = "player rigidbody";

                    this._optitrackRigidBody = this._optitrackRigidbodyTransform.gameObject.AddComponent<OptitrackRigidBody>();
                    if (this._optitrackRigidBody != null)
                        this._optitrackRigidBody.RigidBodyId = this._vRPlayerManager.trackingIndex + 41;
                }

                this._lastPositions = new Vector3[this.recordPositionsLength];
                this._lastOffsets = new Vector3[this.offsetRecordeLength];

                if (!this.isServer)
                {
                    this.CmdUpdateDoLocalCalibration(true);
                }
                else if (this.isLocalPlayer)
                    this.doLocalCalibration = true;
            }

            else if (this.isLocalPlayer && !GlobalVariables.useOptitrackCalibration)
			{
                if (!this.isServer)
                {
                    this.CmdUpdateDoLocalCalibration(true);
                }
                else
                    this.doLocalCalibration = true;
            }
        }

        public override void Start()
        {
            base.Start();
        }

        public override void OnStartLocalPlayer()
        {
            this._vRPlayerManager = this.gameObject.GetComponent<VRPlayerManager>();

            if (!this.isServer && !GlobalVariables.useOptitrackCalibration)
                this.CmdUpdateDoLocalCalibration(true);
        }

        [Command]
        public void CmdUpdateDoLocalCalibration(bool b)
        {
            this.doLocalCalibration = b;
            this.DeInitialize();
        }

        public void OnUpdateLocalCalubration(bool _, bool newValue)
		{
            this.DeInitialize();
		}

        void DeInitialize()
		{
            if (this._initialized)
			{
                if (!this.isLocalPlayer && this.doLocalCalibration || this.isLocalPlayer && !GlobalVariables.useOptitrackCalibration)
				{
                    if (this._optitrackRigidbodyTransform)
                        Destroy(this._optitrackRigidbodyTransform.gameObject);

                    this._initialized = false;
                    Debug.Log("deinitialize " + this.gameObject.name);
                }
            }
		}

        public void UpdateTrackingIndex()
        {
            if (this._vRPlayerManager != null && this._optitrackRigidBody != null)
            {
                this._optitrackRigidBody.RigidBodyId = this._vRPlayerManager.trackingIndex + 41;
                this._optitrackRigidbodyTransform.gameObject.name = "player rigidbody " + this._optitrackRigidBody.RigidBodyId;
            }
        }

        public override void OnDestroy()
        {
            base.OnDestroy();

            if (this._calibrationHead != null)
                Destroy(this._calibrationHead.gameObject);

            if (this._optitrackRigidbodyTransform != null && this._optitrackRigidbodyTransform.gameObject)

                Destroy(this._optitrackRigidbodyTransform.gameObject);
        }

        // Update is called once per frame
        void Update()
        {
            if (this.doLocalCalibration && !this.isLocalPlayer || this.doLocalCalibration && this._lastPositions == null)
                return;

            if (this.isLocalPlayer)
            {
                if (this._isCalibrating)
                {
                    //update ui by local player
                    if (PauseUI.Instance != null)
                        PauseUI.Instance.SetOptitrackCalibrationTimer((Time.time - this._calibrationStartTime) / this.calibrationWaitTime);
                }
            }

            if (!this._initialized)
                return;

            if (this.doLocalCalibration && this.isLocalPlayer && GlobalVariables.useOptitrackCalibration || !this.doLocalCalibration && this.isServer)
            {
                this.RecordePosition();

                if (this._offsetRecognized)
                {
                    //douring calibration
                    //Check if stand still, if true, start calibration
                    if (this.GetVelocity().magnitude < this.calibrationTriggerValue)
                    {
                        if (!this._isCalibrating)
                        {
                            //start calibrating
                            this._isCalibrating = true;
                            this._calibrationStartTime = Time.time;
                            this.SetIsCalibrating(true);
                        }
                        else
                        {
                            if (this._calibrationStartTime + this.calibrationWaitTime < Time.time)
                            {
                                //Set calibration
                                this.Calibrate();
                                this.EndCalibration();
                            }
                        }
                    }
                    else if (this._isCalibrating)
                    {
                        this._isCalibrating = false;
                        this.SetIsCalibrating(false);
                    }
                }

                //Check offset of rigidbody andd player head
                if (this._lastOffsetCheckTime + this.offsetCheckInterval < Time.time)
                {
                    this.RecordeOffset();
                    if (!this._offsetRecognized && this.GetOffset().magnitude > this.CalculateTriggerOffset())
                    {
                        //offset is higher then it shoud
                        this.StartCalibration();
                    }

                    this._lastOffsetCheckTime = Time.time;
                }

                //Debug.Log("current offset " + this.GetOffset().magnitude + "; current velocity " + this.GetVelocity().magnitude);
            }
        }

        
        private float CalculateTriggerOffset()
        {
            return Mathf.Lerp(this.offsetTriggerValue.x, this.offsetTriggerValue.y, Mathf.InverseLerp(this.offsetVelocity.x, this.offsetVelocity.y, this.GetVelocity().magnitude));
        }

        public void StartCalibration()
        {
            this._offsetRecognized = true;
            this.SetIsOffsetRecognized(true);

            this.ResetRecordedVelocity();
        }

        void Calibrate()
        {
            if (this._calibrationCenter == null)
            {
                this._calibrationCenter = new GameObject().transform;
                this._calibrationCenter.gameObject.name = "CalibrationCenter";
            }
            if (this._calibrationHead == null)
            {
                this._calibrationHead = new GameObject().transform;
                this._calibrationHead.gameObject.name = "CalibrationHead";
            }

            if (this._calibrationHead != null && this._calibrationCenter != null)
            {
                this._calibrationCenter.parent = null;
                this._calibrationCenter.position = this._vRPlayerManager.center.position;
                this._calibrationCenter.forward = Vector3.ProjectOnPlane(this._vRPlayerManager.center.forward, Vector3.up);

                this._calibrationHead.parent = null;
                this._calibrationHead.position = this._vRPlayerManager.head.position;
                this._calibrationHead.forward = Vector3.ProjectOnPlane(this._vRPlayerManager.head.forward, Vector3.up);

                this._calibrationCenter.SetParent(this._calibrationHead);
                this._calibrationHead.position = this._optitrackRigidbodyTransform.position;
                this._calibrationHead.forward = Vector3.ProjectOnPlane(this._optitrackRigidbodyTransform.forward, Vector3.up);
                //this._calibrationHead.rotation = this._optitrackRigidbodyTransform.rotation;

                this.SendOffset(this._calibrationCenter.position, this._calibrationCenter.rotation);
            }
        }

        public void SendOffset(Vector3 position, Quaternion rotation)
        {
            if (this.isLocalPlayer && this.doLocalCalibration)
            {
                if (this._vRPlayerManager != null)
                {
                    //Debug.Log("Set is offset: " + position + "; " + rotation);
                    this._vRPlayerManager.center.position = position;
                    this._vRPlayerManager.center.rotation = rotation;
                }
            }
            else if (this.isServer && !this.doLocalCalibration)
                this.RpcSendOffset(this.netIdentity.connectionToClient, position, rotation);
        }

        [TargetRpc]
        public void RpcSendOffset(NetworkConnection network, Vector3 position, Quaternion rotation)
        {
            if (this.isLocalPlayer && this._vRPlayerManager != null && GlobalVariables.useOptitrackCalibration)
            {
                //Debug.Log("Set is offset: " + position + "; " + rotation);
                this._vRPlayerManager.center.position = position;
                this._vRPlayerManager.center.rotation = rotation;
            }
        }

        public void EndCalibration()
        {
            this._offsetRecognized = false;
            this._isCalibrating = false;
            this._lastOffsetCheckTime = Time.time;
            this.SetIsOffsetRecognized(false);
            this.SetIsCalibrating(false);
            this.ResetRecordedOffset();
        }


        void RecordePosition()
        {
            if (this._nextPositionRecordeTime < Time.time && this._lastPositions != null)
            {
                this._lastPositionPointer = (this._lastPositionPointer + 1) % this._lastPositions.Length;
                this._lastPositions[_lastPositionPointer] = this._vRPlayerManager.head.position - this._lastPosition;

                /*
                for (int i = this._lastPositions.Length - 1; i >= 0; i--)
                {
                    if (i == 0)
                        this._lastPositions[i] = this._vRPlayerManager.head.position - this._lastPosition;
                    else
                        this._lastPositions[i] = this._lastPositions[i - 1];
                }*/
                this._lastPosition = this._vRPlayerManager.head.position;
                this._nextPositionRecordeTime = Time.time + (this.recordPositionsTime / this._lastPositions.Length);
            }
        }

        private void ResetRecordedVelocity()
        {
            for (int i = this._lastPositions.Length - 1; i >= 0; i--)
                this._lastPositions[i] = Vector3.zero;
            this._lastPosition = this._vRPlayerManager.head.position;
        }


        public Vector3 GetVelocity()
        {
            Vector3 averageDirection = Vector3.zero;

            for (int i = 1; i < this._lastPositions.Length; i++)
            {
                averageDirection += this._lastPositions[i];
            }
            averageDirection = averageDirection / (this._lastPositions.Length);

            //Debug.Log("Average velocity " + averageDirection.magnitude);
            return averageDirection;
        }

        void RecordeOffset()
        {
            if (this._lastOffsets != null)
			{
                this._offsetPointer = (this._offsetPointer + 1) % this._lastOffsets.Length;
                this._lastOffsets[_offsetPointer] = this._vRPlayerManager.head.position - this._optitrackRigidbodyTransform.position;
			}
        }


        private void ResetRecordedOffset()
        {
            for (int i = this._lastOffsets.Length - 1; i >= 0; i--)
                this._lastOffsets[i] = Vector3.zero;
        }

        public Vector3 GetOffset()
        {
            Vector3 averageOffset = Vector3.zero;

            for (int i = 1; i < this._lastOffsets.Length; i++)
            {
                averageOffset += this._lastOffsets[i];
            }
            averageOffset = averageOffset / (this._lastOffsets.Length);

            //Debug.Log("Average offset " + averageOffset.magnitude);
            return averageOffset;
        }

        public void SetIsOffsetRecognized(bool b)
        {
            if (this.isLocalPlayer && this.doLocalCalibration)
            {
                Debug.Log("Set is offset recognized " + b);
                //change calibration UI
                if (PauseUI.Instance != null)
                    PauseUI.Instance.ActivateOptitrackCalibrationUI(b);
                this._offsetRecognized = b;

                if (PauseUI.Instance != null)
                    PauseUI.Instance.SetOptitrackCalibrationTimer(0);
            }
            else if (this.isServer && !this.doLocalCalibration)
                this.RpcSetIsOffsetRecognized(this.netIdentity.connectionToClient, b);
        }
        [TargetRpc]
        public void RpcSetIsOffsetRecognized(NetworkConnection network, bool b)
        {
            if (this.isLocalPlayer && GlobalVariables.useOptitrackCalibration)
            {
                Debug.Log("Set is offset recognized " + b);
                //change calibration UI
                if (PauseUI.Instance != null)
                    PauseUI.Instance.ActivateOptitrackCalibrationUI(b);
                this._offsetRecognized = b;

                if (PauseUI.Instance != null)
                    PauseUI.Instance.SetOptitrackCalibrationTimer(0);
            }
        }


        public void SetIsCalibrating(bool b)
        {
            if (this.isLocalPlayer && this.doLocalCalibration)
            {
                Debug.Log("Set is calibrating " + b);
                this._isCalibrating = b;
                if (b)
                    this._calibrationStartTime = Time.time;

                if (PauseUI.Instance != null)
                    PauseUI.Instance.SetOptitrackCalibrationTimer(0);
            }
            else if (this.isServer && !this.doLocalCalibration)
                this.RpcSetIsCalibrating(this.netIdentity.connectionToClient, b);
        }

        [TargetRpc]
        public void RpcSetIsCalibrating(NetworkConnection network, bool b)
        {
            if (this.isLocalPlayer && GlobalVariables.useOptitrackCalibration)
            {
                Debug.Log("Set is calibrating " + b);
                this._isCalibrating = b;
                if (b)
                    this._calibrationStartTime = Time.time;

                if (PauseUI.Instance != null)
                    PauseUI.Instance.SetOptitrackCalibrationTimer(0);
            }
        }

        public void OnDrawGizmos()
        {
            if (this._vRPlayerManager != null && this._optitrackRigidbodyTransform != null)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(this._vRPlayerManager.head.position, 0.05f);

                Gizmos.color = Color.red;
                Gizmos.DrawSphere(this._optitrackRigidbodyTransform.position, 0.05f);
            }
        }
    }
}