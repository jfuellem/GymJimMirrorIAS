using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimatorEvents : MonoBehaviour
{
	public UnityEvent OnAnimatorIKEvnets;

	private void OnAnimatorIK(int layerIndex)
	{
		this.OnAnimatorIKEvnets.Invoke();
	}
}
