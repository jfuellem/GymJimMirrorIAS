using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using IAS.CoLocationMUVR;

public class OptitrackAddressUI : MonoBehaviour
{

    public TMP_InputField serverAddressInputField;
    public TMP_InputField localAddressInputField;

    public Toggle startOptitrackConnectionToggle;

    private bool _startOptitrackConnection = false;
    private string _optitrackServerAddress = "127.0.0.1";
    private string _optitrackLocalAddress = "127.0.0.1";

    // Start is called before the first frame update
    void Start()
    {
        if ( Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor ||
                Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer)
        {
            if (PlayerPrefs.HasKey("OptitrackServerAddress"))
            {
                this._optitrackServerAddress = PlayerPrefs.GetString("OptitrackServerAddress");
                GlobalVariables.optitrackServerAddress = this._optitrackServerAddress;

                if (this.serverAddressInputField != null)
                    this.serverAddressInputField.SetTextWithoutNotify(this._optitrackServerAddress);
            }
			else
			{
                this._optitrackServerAddress = GlobalVariables.optitrackServerAddress;

                if (this.serverAddressInputField != null)
                    this.serverAddressInputField.SetTextWithoutNotify(this._optitrackServerAddress);
            }

            if (PlayerPrefs.HasKey("OptitrackLocalAddress"))
            {
                this._optitrackLocalAddress = PlayerPrefs.GetString("OptitrackLocalAddress");
                GlobalVariables.optitrackLocalAddress = this._optitrackLocalAddress;
                if (this.localAddressInputField != null)
                    this.localAddressInputField.SetTextWithoutNotify(this._optitrackLocalAddress);
            }
            else
            {
                this._optitrackLocalAddress = GlobalVariables.optitrackLocalAddress;

                if (this.localAddressInputField != null)
                    this.localAddressInputField.SetTextWithoutNotify(this._optitrackLocalAddress);
            }

            if (PlayerPrefs.HasKey("OptitrackConnectionActive"))
            {
                this._startOptitrackConnection = PlayerPrefs.GetInt("OptitrackConnectionActive") > 0 ? true : false;
                GlobalVariables.startOptitrackConnection = this._startOptitrackConnection;
                if (this.startOptitrackConnectionToggle != null)
                    this.startOptitrackConnectionToggle.SetIsOnWithoutNotify(this._startOptitrackConnection);
            }
            else
            {
                this._startOptitrackConnection = GlobalVariables.startOptitrackConnection;

                if (this.startOptitrackConnectionToggle != null)
                    this.startOptitrackConnectionToggle.SetIsOnWithoutNotify(this._startOptitrackConnection);
            }

        }
        else
            Destroy(this.gameObject);
    }

    public void OnChangeServerAddress(string value)
	{
        this._optitrackServerAddress = value;
        GlobalVariables.optitrackServerAddress = this._optitrackServerAddress;

        PlayerPrefs.SetString("OptitrackServerAddress", this._optitrackServerAddress);
        PlayerPrefs.Save();
    }

    public void OnChangeLocalAddress(string value)
    {
        this._optitrackLocalAddress = value;
        GlobalVariables.optitrackLocalAddress = this._optitrackLocalAddress;

        PlayerPrefs.SetString("OptitrackLocalAddress", this._optitrackLocalAddress);
        PlayerPrefs.Save();
    }

    public void OnOptitrackConnectionChange(bool value)
	{
        this._startOptitrackConnection = value;
        GlobalVariables.startOptitrackConnection = this._startOptitrackConnection;

        PlayerPrefs.SetInt("OptitrackConnectionActive", this._startOptitrackConnection ? 1 : 0);
        PlayerPrefs.Save();
    }
}
