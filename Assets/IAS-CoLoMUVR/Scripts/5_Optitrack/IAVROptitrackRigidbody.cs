using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;


namespace IAS.CoLocationMUVR.Optitrack
{
    public class IAVROptitrackRigidbody : IAVROptitrackObject
    {
        public int id = 0;

        private OptitrackRigidBody _optitrackRigidBody;

        public override void Initialize(bool addOptitrackComponent)
        {
            base.Initialize(addOptitrackComponent);
            if (addOptitrackComponent)
            {
                this._optitrackRigidBody = this.gameObject.AddComponent<OptitrackRigidBody>();
                if (this._optitrackRigidBody != null)
                    this._optitrackRigidBody.RigidBodyId = this.id;

                if (!this.isServer)
                    this.TurnOffSyncOnClients();
            }
        }

        void TurnOffSyncOnClients()
        {
            NetworkTransform networkTransform = this.gameObject.GetComponent<NetworkTransform>();

            if (networkTransform != null)
            {
                networkTransform.syncPosition = false;
                networkTransform.syncRotation = false;
                networkTransform.syncScale = false;
            }
        }
    }
}