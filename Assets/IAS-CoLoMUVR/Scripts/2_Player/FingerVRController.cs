﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

namespace IAS.CoLocationMUVR
{
	public class FingerVRController : VrController
	{
		[Header("Finger Grab")]
		public bool canFingerGrab = true;
		public GrabFingerTrigger thumbFingerTrigger;
		public GrabFingerTrigger[] otherFingersGrabTriggers;
		private int grabingFingerIndex;

		public float velocityMultiplier = 10f;

		private Vector3 _lastPosition;
		private Vector3 _lastRotation;

		private bool _areFingerTrackingComponentsActive = false;

		private bool _collidersInitialized = false;
		public Collider[] fingerColliders;
		private bool _areFingerCollidersActive = false;
		private float _lastEndGrabTime;

		public Mesh mesh;

		public override void Start()
		{
			base.Start();

			this._lastPosition = this._transform.position;
			this._lastRotation = this._transform.rotation.eulerAngles;

			this.ToggleFingerTracking(false);
			this.ToggleFingerColliders(false);
		}


		public override void FixedUpdate()
		{
			base.FixedUpdate();

			//if (!this._collidersInitialized && this.handSkeleton != null && this.handSkeleton.Capsules.Count > 0)
			//	this.InitializeColliders();

			if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer || this.targetVRPlayer == null && !NetworkManager.singleton.isNetworkActive)
			{
				this._lastPosition = this._transform.position;
				this._lastRotation = this._transform.rotation.eulerAngles;
				if (this._doFingerTracking != this._areFingerTrackingComponentsActive && this.canFingerGrab)
					this.ToggleFingerTracking(this._doFingerTracking);

				if (this._doFingerTracking != this._areFingerCollidersActive)
					this.ToggleFingerColliders(this._doFingerTracking);

				if (this._doFingerTracking)
				{
					if (this.indexFingerCollider != null && this.indexFingerCollider.activeInHierarchy)
						this.indexFingerCollider.SetActive(false);

					//if (this.interactingObj.Equals(null) && this._lastEndGrabTime + 0.2f < Time.time && !this._areFingerCollidersActive)
					//	this.ToggleFingerColliders(true);
				}

			}
		}

		/*void InitializeColliders()
		{
			this.fingerColliders = new CapsuleCollider[this.handSkeleton.Capsules.Count];
			for (int i = 0; i < this.handSkeleton.Capsules.Count; i++)
			{
				this.fingerColliders[i] = this.handSkeleton.Capsules[i].CapsuleCollider;
				GameObject obj = new GameObject();
				obj.transform.SetParent(this.fingerColliders[i].transform);
				obj.transform.localPosition = Vector3.zero;
				obj.transform.localRotation = Quaternion.identity;
				obj.transform.localScale = Vector3.one * 0.02f;
				obj.AddComponent<MeshFilter>().mesh = this.mesh;
				obj.AddComponent<MeshRenderer>();
			}

			this._collidersInitialized = true;
		}*/

		void ToggleFingerTracking(bool b)
		{
			this.thumbFingerTrigger.gameObject.SetActive(b);

			foreach (GrabFingerTrigger finger in this.otherFingersGrabTriggers)
				finger.gameObject.SetActive(b);


			this._areFingerTrackingComponentsActive = b;
			//this.ToggleFingerColliders(b);
		}

		void ToggleFingerColliders(bool b)
		{
			if (this.fingerColliders != null)
			{
				foreach (Collider col in this.fingerColliders)
					col.gameObject.SetActive(b);

				this._areFingerCollidersActive = b;
			}
		}

		#region grab
		public override void Grab()
		{
			if (!this._doFingerTracking || !this.canFingerGrab)
			{
				base.Grab();
				return;
			}

			if (this.interactingObj == null)
			{
				this.interactingObj = this.GetGrabableObjectInFingers(out this.grabingFingerIndex);

				//Start grab
				if (this.interactingObj != null)
				{
					if (this.interactingObj.AttachedController() != null)
						this.interactingObj.AttachedController().ForceEndGrab();
					this.StartInteraction(this.interactingObj);
				}

			}

			else
			{
				//Is still in fingers
				if (this.IsObjectStillInFingers())
				{
					//Douring Grab
					this.Drag(this.interactingObj);
				}
				//End Grab
				else
				{
					this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
					this.interactingObj = null;
				}
			}
		}

		public override void ForceEndGrab()
		{
			if (!this._doFingerTracking || !this.canFingerGrab)
			{
				base.ForceEndGrab();
				return;
			}

			if (this.interactingObj != null && this.interactingObj.AttachedController() != null)
			{
				this.thumbFingerTrigger.RemoveFromHoverList(this.interactingObj);
				this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
				Debug.Log("end force");
			}
			this.interactingObj = null;
		}

		IGrabbableObject GetGrabableObjectInFingers(out int index)
		{
			index = 0;

			if (this.thumbFingerTrigger != null)
			{
				for (int i = this.thumbFingerTrigger.triggerGrabeableObjects.Count - 1; i >= 0; i--)
				{
					if (this.thumbFingerTrigger.triggerGrabeableObjects[i].Equals(null))
					{
						//Debug.Log("had null object in hover list");
						this.thumbFingerTrigger.triggerGrabeableObjects.RemoveAt(i);
					}
					else if (this.thumbFingerTrigger.triggerGrabeableObjects[i].GameObject() != null && this.thumbFingerTrigger.triggerGrabeableObjects[i].CanDrag())
					{
						for (int j = 0; j < this.otherFingersGrabTriggers.Length; j++)
						{
							if (this.otherFingersGrabTriggers[j].IsObjectTriggerd(this.thumbFingerTrigger.triggerGrabeableObjects[i]))
							{
								index = j;
								return this.thumbFingerTrigger.triggerGrabeableObjects[i];
							}
						}
					}
				}
			}

			return null;
		}

		bool IsObjectStillInFingers()
		{
			if (this.thumbFingerTrigger != null)
			{
				if (!this.thumbFingerTrigger.IsObjectTriggerd(this.interactingObj))
					return false;
			}

			foreach (GrabFingerTrigger finger in this.otherFingersGrabTriggers)
			{
				if (finger.IsObjectTriggerd(this.interactingObj))
					return true;
			}

			return false;

		}

		public override void Drag(IGrabbableObject target)
		{
			if (!this._doFingerTracking || !this.canFingerGrab)
			{
				base.Drag(target);
				return;
			}

			if (!this.canGrab)
			{
				if (this.interactingObj != null && this.interactingObj.AttachedController() != null)
				{
					this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
					this.interactingObj = null;
				}

				return;
			}

			if (!target.CanDrag())
			{
				this.ForceEndGrab();
				return;
			}
			if (target.AttachedController() && target.IsInteracting() && target.GetRigidbody() && target.GetGrabType() != GrabType.None)
			{
				if (target.GetRigidbody().isKinematic)
				{
					if (target.VelocitiyFactor() > 0)
					{
						this.posDelta = target.AttachedController().GetCenterPoint() + (target.GameObject().transform.position - this.interactionPoint.position);
						//target.GameObject().transform.position = this.posDelta;
						target.GetRigidbody().MovePosition(this.posDelta);
						Debug.Log("Do kinematic movement");
					}
					if (target.RotationFactor() > 0)
					{
						this.rotationDelta = target.AttachedController().transform.rotation * Quaternion.Inverse(this.interactionPoint.localRotation);
						//target.GameObject().transform.rotation = this.rotationDelta;
						target.GetRigidbody().MoveRotation(this.rotationDelta);
						Debug.Log("Do kinematic rotation");
					}
				}
				else
				{
					//Debug.Log("Drag " + target.GameObject().name);
					this.posDelta = target.AttachedController().GetCenterPoint() - this.interactionPoint.position;
					target.GetRigidbody().velocity = this.posDelta * target.VelocitiyFactor() * Time.fixedDeltaTime;

					this.rotationDelta = target.AttachedController().transform.rotation * Quaternion.Inverse(this.interactionPoint.rotation);
					this.rotationDelta.ToAngleAxis(out this.angel, out this.axis);
					if (this.angel > 180)
						this.angel -= 360;
					target.GetRigidbody().angularVelocity = (Time.fixedDeltaTime * this.angel * this.axis) * target.RotationFactor();
				}
			}
		}

		public override void StartInteraction(IGrabbableObject target)
		{
			if (!this._doFingerTracking || !this.canFingerGrab)
			{
				base.StartInteraction(target);
				this.ToggleFingerColliders(false);
			}
			else
			{
				if (!target.CanDrag())
					return;
				if (this.interactionPoint == null)
				{
					this.interactionPoint = new GameObject().transform;
					this.interactionPoint.name = this.gameObject.name + "InteractionPoint";
				}

				//this.ToggleFingerColliders(false);

				target.SetAttachedController(this);
				this.interactionPoint.position = this.ActiveFingerCenterPosition();
				this.interactionPoint.rotation = this._transform.rotation;
				this.interactionPoint.SetParent(target.GameObject().transform, true);
				target.SetIsInteracting(true);
				target.StartGrabing();

				Debug.Log(this.gameObject.name + " Start grab");
			}
		}

		public override Vector3 GetCenterPoint()
		{

			if (!this._doFingerTracking || !this.canFingerGrab)
				return base.GetCenterPoint();

			return this.ActiveFingerCenterPosition();
		}

		public Vector3 ActiveFingerCenterPosition()
		{
			if (this.thumbFingerTrigger != null && this.otherFingersGrabTriggers.Length > this.grabingFingerIndex)
			{
				return (this.thumbFingerTrigger.transform.position + this.otherFingersGrabTriggers[this.grabingFingerIndex].transform.position) / 2f;
			}

			else
				return this._transform.position;
		}

		public override void EndInteraction(IGrabbableObject target)
		{
			if (!this._doFingerTracking || !this.canFingerGrab)
				base.EndInteraction(target);
			else
			{
				if (!target.Equals(null) && this == target.AttachedController())
				{
					this.interactingObj = null;
					if (target.GameObject() != null)
					{
						target.EndGrabing();
						target.SetAttachedController(null);
						target.SetIsInteracting(false);

						if (!target.GetRigidbody().isKinematic)
						{
							target.GetRigidbody().velocity = this.GetVelocity();
							target.GetRigidbody().angularVelocity = this.GetAngularVelocity();
						}
					}
					if (this.interactionPoint != null)
						this.interactionPoint.parent = null;

					//this._lastEndGrabTime = Time.time;
					Debug.Log(this.gameObject.name + " end grab");
				}
			}
		}

		Vector3 GetVelocity()
		{
			return ((this._transform.position - this._lastPosition) / Time.deltaTime) * this.velocityMultiplier;

		}

		public Vector3 GetAngularVelocity()
		{
			return ((this._transform.rotation.eulerAngles - this._lastRotation) / Time.deltaTime) * this.velocityMultiplier;
		}
		#endregion


	} 
}