﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IAS.CoLocationMUVR
{
	public class GrabFingerTrigger : MonoBehaviour
	{
		public List<IGrabbableObject> triggerGrabeableObjects = new List<IGrabbableObject>();

		public MeshRenderer debugMesh;
		private Material _mat;

		private void Start()
		{
			if (debugMesh != null)
				this._mat = this.debugMesh.material;
		}

		public void OnTriggerEnter(Collider other)
		{
			IGrabbableObject collidedObj = other.gameObject.GetComponent(typeof(IGrabbableObject)) as IGrabbableObject;
			if (collidedObj != null)
			{
				this.AddToHoverList(collidedObj);
			}
		}
		
		public void OnTriggerExit(Collider other)
		{
			IGrabbableObject collidedObj = other.gameObject.GetComponent(typeof(IGrabbableObject)) as IGrabbableObject;
			
			if (collidedObj != null)
			{
				this.RemoveFromHoverList(collidedObj);
			}
		}
		
		public void AddToHoverList(IGrabbableObject target)
		{
			if (target != null && !this.triggerGrabeableObjects.Contains(target) && target.CanDrag())
				this.triggerGrabeableObjects.Add(target);

			if (this._mat != null && this.triggerGrabeableObjects.Count > 0)
				this._mat.color = Color.green;
		}
		
		public void RemoveFromHoverList(IGrabbableObject target)
		{
			if (target != null && this.triggerGrabeableObjects.Contains(target))
			{
				this.triggerGrabeableObjects.Remove(target);
			}
			if (this._mat != null && this.triggerGrabeableObjects.Count <= 0)
				this._mat.color = Color.white;
		}
		
		
		public bool IsObjectTriggerd(IGrabbableObject target)
		{
			foreach(IGrabbableObject obj in this.triggerGrabeableObjects)
			{
				if (!obj.Equals(null) && obj == target && target.CanDrag())
					return true;
			}
			return false;
		}
	}
}