using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.XR;
using UnityEngine.Events;
using Mirror;

namespace IAS.CoLocationMUVR
{
	public class VrController : MonoBehaviour {
		public bool canGrab = true;
		protected Transform _transform;

		private Vector3 _velocity;
		public Vector3 velocity { get { return this._velocity; } }

		private Vector3 _lastPosition;
		
		public VRPlayerManager targetVRPlayer;
		public OVRInput.Controller m_controller;
		public OVRInput.Controller m_hand;
		public HandState currentHandState;
		public float indexValue;
		public float grabValue;
		public float thumbValue;

		private OVRHand _ovrHand;
		
		public Animator animatorComp;
		private string _grabAnimationParameterName;
		
		public Transform noTrackingIdlePosition; //Override position if the oculus quest finger tracking dosnt work

		private Vector3 _previousPosition;
		
		List<IGrabbableObject> objectsHoveringOver = new List<IGrabbableObject>();
		public IGrabbableObject closestObj;
		private IGrabbableObject _lastClosestObj;
		public IGrabbableObject interactingObj;


		public GameObject indexFingerCollider;
		
		private bool _isGrapDown = false;

		public Transform handCenterPoint;

		//Drag
		public Transform interactionPoint;
		protected Vector3 posDelta;
		
		protected Quaternion rotationDelta;
		protected float angel;
		protected Vector3 axis;

		public bool doClosestHoverObjectActions = false;

		public UnityEvent startGrabEvents;
		public UnityEvent onGrabEvents;
		public UnityEvent endGrabEvents;


		[Header("Finger Tracking Aproximation")]

		public OVRSkeleton handSkeleton;
		public Finger[] fingers;
		protected bool _doFingerTracking;
		public bool DoFingerTracking() { return this._doFingerTracking; }
		private float _lastFingerValueUpdateTime;
		[Range(0f,1f)]
		public float fingerTrackingSyncInterval = 0.1f;
		
		public Transform ikOffsetObject;
		public Vector3 offsetControllerPosition;
		public Vector3 offsetControllerRotation;
		public Vector3 offsetHandPosition;
		public Vector3 offsetHandRotation;
		
		[System.Serializable]
		public class Finger
		{
			public Transform bone0;
			public Transform bone1;
			public Transform bone2;
			public float aAngleValue = 720f;
			public float bAngleValue = 520f;
			public float aReMapAngleValue = 720f;
			public float bReMapAngleValue = 520f;
			
			private float _angleValue;
			
			private Quaternion _networkRotation0;
			private float _networkAngleZ1;
			private float _networkAngleZ2;
			
			public float lerpValue = 0.4f;
			
			public float CalculateAngleValue()
			{
				if (bone1 != null && bone2 != null)
				{
					float currentAngleValue = bone1.localEulerAngles.z;
					
					if (currentAngleValue < 180f)
						currentAngleValue += 360f;
					
					currentAngleValue += bone2.localEulerAngles.z;
					if (bone2.localEulerAngles.z < 180f)
						currentAngleValue += 360f;
					
					this._angleValue = Mathf.InverseLerp(aAngleValue, bAngleValue, currentAngleValue);
				}
				
				return _angleValue;
			}
			
			public Quaternion GetNetworkRotation0() { return this._networkRotation0; }
			public float GetAngleValue() { return this._angleValue; }
			
			
			public void SmoothFingerFollow()
			{
				//this.bone0.localRotation = Quaternion.Slerp(this.bone0.localRotation, this._networkRotation0, this.lerpValue * 1.6f);
				this.bone0.localRotation = this._networkRotation0;
				this.bone1.localEulerAngles =  new Vector3(this.bone1.localEulerAngles.x, this.bone1.localEulerAngles.y,
				                                           Mathf.LerpAngle(this.bone1.localEulerAngles.z, this._networkAngleZ1, this.lerpValue));
				
				this.bone2.localEulerAngles = new Vector3(this.bone2.localEulerAngles.x, this.bone2.localEulerAngles.y,
				                                          Mathf.LerpAngle(this.bone2.localEulerAngles.z, this._networkAngleZ2, this.lerpValue));
			}
			
			public void SetData(Quaternion newRotation, float angelValue, OVRInput.Controller targetController)
			{
				this._networkRotation0 = newRotation;
				this._angleValue = angelValue;
				//float reMapedAngle = Mathf.Lerp(0f - this.angleReMapValue, 1f + this.angleReMapValue, this._angleValue);
				
				float angle1 = Mathf.Lerp(aReMapAngleValue, bReMapAngleValue, this._angleValue) / 2f;
				
				float angle2 = angle1;
				
				if (targetController == OVRInput.Controller.LHand)
				{
					angle1 -= 5f;
					angle2 += 5f;
				}
				else
				{
					angle1 += 5f;
					angle2 -= 5f;
				}
				
				if (angle1 >= 360f)
					angle1 -= 360f;
				if (angle2 >= 360f)
					angle2 -= 360f;
				
				this._networkAngleZ1 = angle1;
				this._networkAngleZ2 = angle2;
			}
		}
		
		
		public virtual void Start()
		{
            Init();
		}

        private bool _init;
        private void Init()
        {
            if (_init)
                return;

            this._transform = this.transform;
            this.SetIKOffsetObject();

            this._ovrHand = this.gameObject.GetComponent(typeof(OVRHand)) as OVRHand;

            _init = true;
        }

        public virtual void FixedUpdate() {
			
			if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer || this.targetVRPlayer == null && !NetworkManager.singleton.isNetworkActive)
			{
				this.UpdateVelocity();

				if (OVRInput.IsControllerConnected(m_controller))
					this.UpdateHandAnimationState();
				else if (!OVRInput.IsControllerConnected(this.m_controller) && this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer && this._doFingerTracking)
					this.UpdateFingerValuesLocal();
				
				this.Grab();
				/*
				if (this.animatorComp != null)
				{
					int i = 1;
					if (this.m_controller == OVRInput.Controller.RTouch)
						i = 4;
					
					if (OVRInput.GetActiveController() == OVRInput.Controller.Touch && this.animatorComp.GetLayerWeight(i) < 1f)
					{
						this.animatorComp.SetLayerWeight(i, 1f);
						this.animatorComp.SetLayerWeight(i+1, 1f);
						this.animatorComp.SetLayerWeight(i+2, 1f);
						this._doFingerTracking = false;
						this.SetIKOffsetObject();
						//this.animatorComp.enabled = true;
					}
					else if (OVRInput.GetActiveController() != OVRInput.Controller.Touch && this.animatorComp.GetLayerWeight(i) > 0f)
					{
						this.animatorComp.SetLayerWeight(i, 0f);
						this.animatorComp.SetLayerWeight(i+1, 0f);
						this.animatorComp.SetLayerWeight(i+2, 0f);
						this._doFingerTracking = true;
						Debug.Log("Finger tracking true by FixedUpdate " + this.m_controller);
						this.SetIKOffsetObject();
						//this.animatorComp.enabled = false;
					}
				}
				*/
			}
		}
		
		private void LateUpdate()
		{
			/*
			if (this.animatorComp != null && this.targetVRPlayer != null )
			{
				int i = 1;
				if (this.m_controller == OVRInput.Controller.RTouch)
					i = 4;
				
				if (!this._doFingerTracking)
				{
					if (this.animatorComp.GetLayerWeight(i) < 1f)
					{
						this.animatorComp.SetLayerWeight(i, 1f);
						this.animatorComp.SetLayerWeight(i+1, 1f);
						this.animatorComp.SetLayerWeight(i+2, 1f);
						//this.animatorComp.enabled = true;
					}
				}
				else if (this.animatorComp.GetLayerWeight(i) > 0f)
				{
					this.animatorComp.SetLayerWeight(i, 0f);
					this.animatorComp.SetLayerWeight(i+1, 0f);
					this.animatorComp.SetLayerWeight(i+2, 0f);
					//this.animatorComp.enabled = false;
				}
			}

			//Write the finger tracking to data
			if (this.targetVRPlayer != null && !this.targetVRPlayer.isLocalPlayer && this._doFingerTracking)
			{
				foreach (Finger finger in this.fingers)
					finger.SmoothFingerFollow();
			}
			*/
		}
		
		public void UpdateTransform()
		{
            Init();

            //Debug.Log(OVRInput.GetActiveController());
            if (OVRInput.GetActiveController() == OVRInput.Controller.Touch || OVRInput.GetActiveController() == this.m_controller)
			{
				if (OVRInput.GetControllerPositionValid(this.m_controller))
				{
					this._transform.localPosition = OVRInput.GetLocalControllerPosition(this.m_controller);
					this._transform.localRotation = OVRInput.GetLocalControllerRotation(this.m_controller);
				}
			}
			else
			{
				if (PauseUI.Instance != null && PauseUI.Instance.calibrationGameObject && PauseUI.Instance.calibrationGameObject.activeInHierarchy)
				{
					//No Update of hand position
				}
				else
				{
					if (OVRInput.GetControllerPositionTracked(this.m_controller) && OVRInput.GetActiveController() == OVRInput.Controller.Hands)
					{
						this._transform.localPosition = OVRInput.GetLocalControllerPosition(m_controller);
						this._transform.localRotation = OVRInput.GetLocalControllerRotation(m_controller);
					}
					else if (this.noTrackingIdlePosition != null)
					{
						this._transform.position = Vector3.Lerp(this._transform.position, this.noTrackingIdlePosition.position, 0.2f);
						this._transform.rotation = Quaternion.Slerp(this._transform.rotation, this.noTrackingIdlePosition.rotation, 0.1f);
					}
				}
			}
			
			
		}

		void UpdateVelocity()
		{
			this._velocity = this._transform.position - this._lastPosition;

			this._lastPosition = this._transform.position;
		}

		public void ChangeAnimator(Animator target)
		{
			this.animatorComp = target;
		}

		void SetIKOffsetObject()
		{
			if (this.ikOffsetObject != null)
			{
				if (this._doFingerTracking)
				{
					this.ikOffsetObject.localPosition = this.offsetHandPosition;
					this.ikOffsetObject.localRotation = Quaternion.Euler(this.offsetHandRotation);
				}
				else
				{
					this.ikOffsetObject.localPosition = this.offsetControllerPosition;
					this.ikOffsetObject.localRotation = Quaternion.Euler(this.offsetControllerRotation);
				}
			}
		}
		
		
		void UpdateHandAnimationState()
		{
			this.indexValue = 0;
			this.grabValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, this.m_controller);
			this.thumbValue = 0;

            //Index
            if (OVRInput.Get(OVRInput.Touch.PrimaryIndexTrigger, this.m_controller))
            {
                this.indexValue = 0.5f + (OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, this.m_controller) / 2);
            }

            if (!OVRInput.Get(OVRInput.Touch.One, this.m_controller) && !OVRInput.Get(OVRInput.Touch.Two, this.m_controller) &&
				!OVRInput.Get(OVRInput.Touch.PrimaryThumbstick, this.m_controller))
			{
				this.thumbValue = 1f;
			}
			else
			{
				this.thumbValue = Mathf.Lerp(0.5f, 0f, grabValue);
			}

			/*
			if (!OVRInput.Get(OVRInput.Touch.One, this.m_controller) && !OVRInput.Get(OVRInput.Touch.Two, this.m_controller) && 
			    !OVRInput.Get(OVRInput.Touch.PrimaryThumbstick, this.m_controller) && OVRInput.Get(OVRInput.Touch.PrimaryIndexTrigger, this.m_controller))
			{
				if (this.currentHandState != HandState.Thumbup)
				{
					this.currentHandState = HandState.Thumbup;
					if (this.animatorComp != null)
						this.animatorComp.SetInteger(this.m_controller.ToString(), (int)this.currentHandState);
					
					if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer)
						this.targetVRPlayer.CmdChangeHandState(this.m_controller == OVRInput.Controller.RTouch ? true : false, this.currentHandState);
				}
			}
			else if (!OVRInput.Get(OVRInput.NearTouch.PrimaryIndexTrigger, this.m_controller))
			{
				if (this.currentHandState != HandState.Pointing)
				{
					this.currentHandState = HandState.Pointing;
					if (this.animatorComp != null)
						this.animatorComp.SetInteger(this.m_controller.ToString(), (int)this.currentHandState);
					
					if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer)
						this.targetVRPlayer.CmdChangeHandState(this.m_controller == OVRInput.Controller.RTouch ? true : false, this.currentHandState);
				}
			}
			else if (OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, this.m_controller) > 0.5f
			         || OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, this.m_controller) > 0.5f)
			{
				if (this.currentHandState != HandState.Fist)
				{
					this.currentHandState = HandState.Fist;
					if (this.animatorComp != null)
						this.animatorComp.SetInteger(this.m_controller.ToString(), (int)this.currentHandState);
					
					if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer)
						this.targetVRPlayer.CmdChangeHandState(this.m_controller == OVRInput.Controller.RTouch ? true : false, this.currentHandState);
				}
			}
			else
			{
				if (this.currentHandState != HandState.Idle)
				{
					this.currentHandState = HandState.Idle;
					if (this.animatorComp != null)
						this.animatorComp.SetInteger(this.m_controller.ToString(), (int)this.currentHandState);
					
					if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer)
						this.targetVRPlayer.CmdChangeHandState(this.m_controller == OVRInput.Controller.RTouch ? true : false, this.currentHandState);
				}
			}

			if (this.animatorComp != null)
			{
				this.animatorComp.SetFloat(this.m_controller.ToString() +"_Index", this.indexValue);
				this.animatorComp.SetFloat(this.m_controller.ToString() + "_Grab", this.grabValue);
				this.animatorComp.SetFloat(this.m_controller.ToString() + "_Thumb", this.thumbValue);
			}
			if (this.targetVRPlayer != null && this.targetVRPlayer.isLocalPlayer)
				this.targetVRPlayer.ChangeHandAnimation(this.m_controller == OVRInput.Controller.RTouch ? true : false,  new Vector3(this.indexValue, this.grabValue, this.thumbValue));

			this.ActivateIndexCollider(this.indexValue < 0.1f ? true : false);
			*/
		}

		void ActivateIndexCollider(bool b)
		{
			if (this.indexFingerCollider != null && this.indexFingerCollider.activeInHierarchy != b)
				this.indexFingerCollider.SetActive(b);
		}

		public void UpdateAnimationValues(Vector3 value)
		{
			if (this._doFingerTracking)
			{
				this._doFingerTracking = false;
				this.SetIKOffsetObject();
			}
			/*
			if (this.animatorComp != null)
			{
				this.animatorComp.SetFloat(this.m_controller.ToString() + "_Index", value.x);
				this.animatorComp.SetFloat(this.m_controller.ToString() + "_Grab", value.y);
				this.animatorComp.SetFloat(this.m_controller.ToString() + "_Thumb", value.z);
			}
			*/
		}

		public void UpdateHandState(HandState state)
		{
			if (this._doFingerTracking)
			{
				this._doFingerTracking = false;
				this.SetIKOffsetObject();
			}
			/*
			if (this.currentHandState != state)
			{
				this.currentHandState = state;
				if (this.animatorComp != null)
					this.animatorComp.SetInteger(this.m_controller.ToString(), (int)this.currentHandState);
			}
			*/
		}

		public virtual Vector3 GetCenterPoint()
		{
			if (this.handCenterPoint != null)
				return this.handCenterPoint.position;
			else
				return this._transform.position;
		}

		#region grab
		public virtual void Grab()
		{
			if (!this.canGrab)
			{
				if (this.interactingObj != null && this.interactingObj.AttachedController() != null)
				{
					this._isGrapDown = false;
					this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
					this.closestObj = null;
					this.interactingObj = null;
				}

				return;
			}

			if (!this._isGrapDown)
			{
				if (this._lastClosestObj != this.closestObj)
					this._lastClosestObj = this.closestObj;

				if (this.objectsHoveringOver.Count > 0)
				{
					float minDistance = float.MaxValue;
					float distance;

					for (int i = this.objectsHoveringOver.Count - 1; i >= 0; i--)
					{
						if (this.objectsHoveringOver[i].Equals(null) || !this.objectsHoveringOver[i].GameObject().activeInHierarchy)
						{
							this.objectsHoveringOver.RemoveAt(i);
						}
						else if (this.objectsHoveringOver[i].GameObject() != null && this.objectsHoveringOver[i].CanDrag())
						{
							distance = Vector3.Distance(this._transform.position, this.objectsHoveringOver[i].GameObject().transform.position);
							if (distance < minDistance)
							{
								minDistance = distance;
								this.closestObj = this.objectsHoveringOver[i];
							}
						}
					}
				}
				else
				{
					this.closestObj = null;
				}
				//Hover effects
				if (this.doClosestHoverObjectActions)
				{
					if (this._lastClosestObj != null && this._lastClosestObj != this.closestObj)
						this._lastClosestObj.OnEndClosestObject();
					if (this.closestObj != null && this.closestObj != this._lastClosestObj)
						this.closestObj.OnStartClosestObject();
				}

				if (this.closestObj != null && OVRInput.GetDown(OVRInput.Button.One, this.m_controller))
				{
					this.closestObj.IsClosestButtonEvent(this);
				}
			}



			if (this.GrabStrength() > 0.6f)
			{
				//Start Grab
				if (this._isGrapDown == false)
				{

					this._isGrapDown = true;
					this.startGrabEvents.Invoke();

					this.interactingObj = this.closestObj;
					if (this.interactingObj != null)
					{
						if (this.interactingObj.IsInteracting() && this.interactingObj.AttachedController() != null)
						{
							Debug.Log("End grab on other hand ");
							this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
						}
						Debug.Log("Starg grab on Hand");
						this.StartInteraction(this.interactingObj);
						Debug.Log(this.interactingObj.IsInteracting());
					}
				}

				//During Grab
				else if (this._isGrapDown)
				{
					this.onGrabEvents.Invoke();
					if (this.interactingObj != null)
						this.Drag(interactingObj);
				} 
				
			}
			//End Grab
			else if (this.GrabStrength() <= 0.6f)
			{
				if (this._isGrapDown)
				{
					this._isGrapDown = false;

					this.endGrabEvents.Invoke();
				}


				if (this.interactingObj != null && this.interactingObj.AttachedController() != null)
				{
					this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
					this.closestObj = null;
					this.interactingObj = null;
				}
			}
		}

		//Use this to get the strength of the grab and trigger button strength of the controller and Index pinch strength with Handtracking
		public float GrabStrength()
		{
			if (this._doFingerTracking && this._ovrHand != null)
			{
				float indexValue = this._ovrHand.GetFingerPinchStrength(OVRHand.HandFinger.Index);
				float middleValue = this._ovrHand.GetFingerPinchStrength(OVRHand.HandFinger.Middle);

				return indexValue > middleValue ? indexValue : middleValue;
			}
			else
			{
				float gripValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, this.m_controller);
				float indexValue = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, this.m_controller);
				
				return gripValue > indexValue ? gripValue : indexValue;
			}
		}

		public void ChangeGrabTarget(IGrabbableObject currentTarget, IGrabbableObject newTarget)
		{
			if (this.interactingObj == currentTarget)
			{
				this.interactingObj = newTarget;
				this.StartInteraction(newTarget);
			}
		}

		public virtual void ForceEndGrab()
		{
			Debug.Log("force end grab");
			if (this.interactingObj != null && this.interactingObj.AttachedController() != null)
			{
				this.interactingObj.AttachedController().EndInteraction(this.interactingObj);
			}
			this.closestObj = null;
			this.interactingObj = null;
			
			objectsHoveringOver = new List<IGrabbableObject>();
		}
		
		public void OnTriggerEnter(Collider other)
		{
			IGrabbableObject collidedObj = other.gameObject.GetComponent(typeof(IGrabbableObject)) as IGrabbableObject;
			if (collidedObj != null)
			{
				this.AddToHoverList(collidedObj);
			}
		}
		
		public void OnTriggerExit(Collider other)
		{
			IGrabbableObject collidedObj = other.gameObject.GetComponent(typeof(IGrabbableObject)) as IGrabbableObject;
			
			if (collidedObj != null)
			{
				this.RemoveFromHoverList(collidedObj);
			}
		}
		
		public void AddToHoverList(IGrabbableObject target)
		{
			if (target != null && !this.objectsHoveringOver.Contains(target) && target.CanDrag())
				this.objectsHoveringOver.Add(target);
		}
		
		public void RemoveFromHoverList(IGrabbableObject target)
		{
			if (target != null && this.objectsHoveringOver.Contains(target))
			{
				this.objectsHoveringOver.Remove(target);
			}
		}
		
		
		public virtual void Drag(IGrabbableObject target)
		{
			if (!target.CanDrag())
			{
				this.ForceEndGrab();
				return;
			}
			if (target.AttachedController() && target.IsInteracting() && target.GetRigidbody())
			{
				if (target.GetGrabType() == GrabType.Velocity)
				{
					//Debug.Log("Drag " + target.GameObject().name);
					this.posDelta = target.AttachedController().transform.position - this.interactionPoint.position;
					target.GetRigidbody().velocity = this.posDelta * target.VelocitiyFactor() * Time.fixedDeltaTime;

					this.rotationDelta = target.AttachedController().transform.rotation * Quaternion.Inverse(this.interactionPoint.rotation);
					this.rotationDelta.ToAngleAxis(out this.angel, out this.axis);
					if (this.angel > 180)
						this.angel -= 360;
					target.GetRigidbody().angularVelocity = (Time.fixedDeltaTime * this.angel * this.axis) * target.RotationFactor();
				}
				else if (target.GetGrabType() == GrabType.Instantly)
				{
					target.GameObject().transform.position = this.interactionPoint.position;
					target.GameObject().transform.rotation = this.interactionPoint.rotation;
				}
			}
		}
		
		public virtual void StartInteraction(IGrabbableObject target)
		{
			if (!target.CanDrag())
				return;
			if (this.interactionPoint == null)
			{
				this.interactionPoint = new GameObject().transform;
				this.interactionPoint.name = this.gameObject.name + "InteractionPoint";
			}

			//Hover end effects
			if (this.doClosestHoverObjectActions)
				target.OnEndClosestObject();

			target.SetAttachedController(this);

			if (target.GetGrabType() == GrabType.Velocity)
			{
				this.interactionPoint.position = target.SnapToCenter() ? target.GameObject().transform.position : this._transform.position;
				this.interactionPoint.rotation = target.SnapToCenter() ? target.GameObject().transform.rotation : this._transform.rotation;
				this.interactionPoint.SetParent(target.GameObject().transform, true);
			}
			else if (target.GetGrabType() == GrabType.Instantly)
			{
				this.interactionPoint.position = target.SnapToCenter() ? this._transform.position : target.GameObject().transform.position;
				this.interactionPoint.rotation = target.SnapToCenter() ? this._transform.rotation : target.GameObject().transform.rotation;
				this.interactionPoint.SetParent(this._transform, true);
			}

			target.SetIsInteracting(true);
			target.StartGrabing();
			this.PlayGrabAudio();
		}
		
		public virtual void EndInteraction(IGrabbableObject target)
		{
			if (target != null && this == target.AttachedController())
			{
				this.interactingObj = null;
				if (target.GameObject() != null)
				{
					target.EndGrabing();
					target.SetAttachedController(null);
					target.SetIsInteracting(false);
				}
				if (this.interactionPoint != null)
					this.interactionPoint.parent = null;
			}
			
		}
		
		void PlayGrabAudio()
		{
			/*
        if (this.audioSource != null && this.grapAudioClip != null)
        {
            this.audioSource.PlayOneShot(this.grapAudioClip, 0.2f);
        }*/
		}
		
		#endregion
		
		public void SetVribration(float frequency, float amplitude, float duration)
		{
			OVRInput.SetControllerVibration(frequency, amplitude, this.m_controller);
			
			this.StopCoroutine(this.VribrationStop(0f));
			this.StartCoroutine(this.VribrationStop(duration));
		}
		
		IEnumerator VribrationStop(float duration)
		{
			yield return new WaitForSeconds(duration);
			OVRInput.SetControllerVibration(0f, 0f, this.m_controller);
		}
		
		#region Fingertracking
		
		void UpdateFingerValuesLocal()
		{
			foreach (Finger f in this.fingers)
				f.CalculateAngleValue();
			
			if (this._lastFingerValueUpdateTime < Time.time)
			{
				for(int i = 0; i < this.fingers.Length; i++)
				{
					this.targetVRPlayer.CmdUpdateFingerDataOnServer(this.m_controller == OVRInput.Controller.RTouch ? true : false, i, this.fingers[i].bone0.localRotation, this.fingers[i].GetAngleValue());
				}
				this._lastFingerValueUpdateTime = Time.time + this.fingerTrackingSyncInterval;
			}
		}
		
		public void UpdateFingerValuesNetworking(int index, Quaternion rotation, float value)
		{
			if (!this._doFingerTracking)
			{

				this._doFingerTracking = true;
				this.SetIKOffsetObject();
			}
			
			if (index >= 0 && index <= this.fingers.Length)
			{
				this.fingers[index].SetData(rotation, value, this.m_controller);
			}
		}
		
		
		#endregion
	}
	
	public enum HandState {Idle = 0, Pointing = 1, Fist = 2, Thumbup = 3 }
}
