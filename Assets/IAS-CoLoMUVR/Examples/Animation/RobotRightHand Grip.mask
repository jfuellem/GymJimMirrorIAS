%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: RobotRightHand Grip
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: rig_men
    m_Weight: 0
  - m_Path: rig_men/hips
    m_Weight: 0
  - m_Path: rig_men/hips/spine
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/neck
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/neck/head
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/neck/head/head_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/neck/head/head_end/head_end_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/forearm_stub.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/forearm_stub.L/forearm_stub.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/index1.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/index1.L/index2.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/index1.L/index2.L/index3.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/index1.L/index2.L/index3.L/index_null.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/index1.L/index2.L/index3.L/index_null.L/index_null.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/middle1.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/middle1.L/middle2.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/middle1.L/middle2.L/middle3.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/middle1.L/middle2.L/middle3.L/middle_null.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/middle1.L/middle2.L/middle3.L/middle_null.L/middle_null.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/pinky0.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/pinky0.L/pinky1.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/pinky0.L/pinky1.L/pinky2.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/pinky0.L/pinky1.L/pinky2.L/pinky3.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/pinky0.L/pinky1.L/pinky2.L/pinky3.L/pinky_null.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/pinky0.L/pinky1.L/pinky2.L/pinky3.L/pinky_null.L/pinky_null.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/ring1.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/ring1.L/ring2.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/ring1.L/ring2.L/ring3.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/ring1.L/ring2.L/ring3.L/ring_null.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/ring1.L/ring2.L/ring3.L/ring_null.L/ring_null.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/thumb0.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/thumb0.L/thumb1.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/thumb0.L/thumb1.L/thumb2.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/thumb0.L/thumb1.L/thumb2.L/thumb3.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/thumb0.L/thumb1.L/thumb2.L/thumb3.L/thumb_null.L
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.L/upper_arm.L/forearm.L/wrist.L/thumb0.L/thumb1.L/thumb2.L/thumb3.L/thumb_null.L/thumb_null.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/forearm_stub.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/forearm_stub.R/forearm_stub.R_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/index1.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/index1.R/index2.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/index1.R/index2.R/index3.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/index1.R/index2.R/index3.R/index_null.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/index1.R/index2.R/index3.R/index_null.R/index_null.R_end
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/middle1.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/middle1.R/middle2.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/middle1.R/middle2.R/middle3.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/middle1.R/middle2.R/middle3.R/middle_null.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/middle1.R/middle2.R/middle3.R/middle_null.R/middle_null.R_end
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/pinky0.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/pinky0.R/pinky1.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/pinky0.R/pinky1.R/pinky2.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/pinky0.R/pinky1.R/pinky2.R/pinky3.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/pinky0.R/pinky1.R/pinky2.R/pinky3.R/pinky_null.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/pinky0.R/pinky1.R/pinky2.R/pinky3.R/pinky_null.R/pinky_null.R_end
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/ring1.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/ring1.R/ring2.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/ring1.R/ring2.R/ring3.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/ring1.R/ring2.R/ring3.R/ring_null.R
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/ring1.R/ring2.R/ring3.R/ring_null.R/ring_null.R_end
    m_Weight: 1
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/thumb0.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/thumb0.R/thumb1.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/thumb0.R/thumb1.R/thumb2.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/thumb0.R/thumb1.R/thumb2.R/thumb3.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/thumb0.R/thumb1.R/thumb2.R/thumb3.R/thumb_null.R
    m_Weight: 0
  - m_Path: rig_men/hips/spine/chest/shoulder.R/upper_arm.R/forearm.R/wrist.R/thumb0.R/thumb1.R/thumb2.R/thumb3.R/thumb_null.R/thumb_null.R_end
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.L
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.L/shin.L
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.L/shin.L/foot.L
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.L/shin.L/foot.L/toe.L
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.L/shin.L/foot.L/toe.L/toe.L_end
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.L/shin.L/foot.L/toe.L/toe.L_end/toe.L_end_end
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.R
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.R/shin.R
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.R/shin.R/foot.R
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.R/shin.R/foot.R/toe.R
    m_Weight: 0
  - m_Path: rig_men/hips/thigh.R/shin.R/foot.R/toe.R/toe.R_end
    m_Weight: 0
  - m_Path: robot_wHead
    m_Weight: 0
