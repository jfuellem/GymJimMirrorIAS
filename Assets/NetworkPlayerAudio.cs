using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Mirror;
using FMODUnity;

[RequireComponent(typeof(StudioEventEmitter))]
public class NetworkPlayerAudio : MonoBehaviour
{

	private static int numPlayers = 0;
	private static List<NetworkIdentity> netIdentities = new();
	private Vector3 prevHeadPos;
	private float headSpeed;
	private float headRotation;
	private float headRotationSpeed;
	private float prevHeadRot;
	public float[] distancesToPlayers = new float[0];
	public Transform cameraTransform;
	public NetworkIdentity ownIdentity;
	//public ListVariable playerList;


	private StudioEventEmitter personalEmitter;
	private FMOD.Studio.PARAMETER_ID param1ID;
	private FMOD.Studio.PARAMETER_ID param2ID;
	private FMOD.Studio.PARAMETER_ID param3ID;

	public delegate void PlayerNumChangedHandler();
	public static event PlayerNumChangedHandler OnPlayerNumChanged;

	private static int playerNum
    {
        get
        {
			return numPlayers;
        }
        set
        {
			numPlayers = value;
			OnPlayerNumChanged?.Invoke();
        }
    }

    private void Start()
    {
		OnPlayerNumChanged += PlayerNumChange;
		netIdentities.Add(ownIdentity);
		playerNum += 1;

		personalEmitter = gameObject.GetComponent<StudioEventEmitter>();
		/*
		FMOD.Studio.EventDescription eventDescription;
		personalEmitter.EventInstance.getDescription(out eventDescription);
		FMOD.Studio.PARAMETER_DESCRIPTION param1descr;
		FMOD.Studio.PARAMETER_DESCRIPTION param2descr;
		FMOD.Studio.PARAMETER_DESCRIPTION param3descr;
		eventDescription.getParameterDescriptionByName("", out param1descr);
		eventDescription.getParameterDescriptionByName("", out param2descr);
		eventDescription.getParameterDescriptionByName("", out param3descr);

		param1ID = param1descr.id;
		param2ID = param2descr.id;
		param3ID = param3descr.id;
		*/
	}

	private void OnDestroy()
	{
		OnPlayerNumChanged -= PlayerNumChange;
		netIdentities.Remove(ownIdentity);
		playerNum -= 1;
	}

	private void PlayerNumChange()
    {
		Array.Resize(ref distancesToPlayers, playerNum - 1);
		Debug.Log("The player list has a length of: " + netIdentities.Count);
	}

	private void FixedUpdate()
	{
		if (cameraTransform != null)
		{
			headSpeed = (cameraTransform.position - prevHeadPos).magnitude / Time.fixedDeltaTime;
			headRotation = cameraTransform.rotation.eulerAngles.y;
			headRotationSpeed = (headRotation - prevHeadRot) / Time.fixedDeltaTime;
			prevHeadPos = cameraTransform.position;
			prevHeadRot = headRotation;

			int i = 0;
			foreach (NetworkIdentity netIdentity in netIdentities)
			{
				if (netIdentity == ownIdentity)
					continue;
				distancesToPlayers[i] = Vector3.Distance(cameraTransform.position, netIdentity.GetComponentInChildren<Camera>().transform.position);
				i++;
			}
		}
	}
}
