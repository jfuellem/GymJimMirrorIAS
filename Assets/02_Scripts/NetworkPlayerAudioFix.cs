using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using FMODUnity;
using System;
using IAS.CoLocationMUVR;
using System.Linq;

public class NetworkPlayerAudioFix : MonoBehaviour
{
	private static int numPlayers = 0;
	private static List<NetworkIdentity> netIdentities = new();
	private Vector3 prevHeadPos;
	private float headSpeed;
	private float headRotation;
	private float headRotationSpeed;
	private float prevHeadRot;
	public float[] distancesToPlayers = new float[0];
	public Transform cameraTransform;
	public NetworkIdentity ownIdentity;
	public VRPlayerManager VRPlayerManager;
	//public ListVariable playerList;

	bool initialized;


	private StudioEventEmitter personalEmitter;
	private FMOD.Studio.PARAMETER_ID ampFollowID;
	private FMOD.Studio.PARAMETER_ID playerIndexID;
	private FMOD.Studio.PARAMETER_ID headSpeedID;
	private FMOD.Studio.PARAMETER_ID headRotID;
	private FMOD.Studio.PARAMETER_ID nearestPlayerID;

	public delegate void PlayerNumChangedHandler();
	public static event PlayerNumChangedHandler OnPlayerNumChanged;

	private static int playerNum
	{
		get
		{
			return numPlayers;
		}
		set
		{
			numPlayers = value;
			OnPlayerNumChanged?.Invoke();
		}
	}

	public void DoShit()
	{
		OnPlayerNumChanged += PlayerNumChange;
		netIdentities.Add(ownIdentity);
		playerNum += 1;

		

		personalEmitter = gameObject.GetComponent<StudioEventEmitter>();
		
		FMOD.Studio.EventDescription eventDescription;
		personalEmitter.EventInstance.getDescription(out eventDescription);
		FMOD.Studio.PARAMETER_DESCRIPTION param1descr;
		FMOD.Studio.PARAMETER_DESCRIPTION param2descr;
		FMOD.Studio.PARAMETER_DESCRIPTION param3descr;
		FMOD.Studio.PARAMETER_DESCRIPTION param4descr;
		FMOD.Studio.PARAMETER_DESCRIPTION param5descr;
		eventDescription.getParameterDescriptionByName("AmplitudeFollow", out param1descr);
		eventDescription.getParameterDescriptionByName("PlayerIndex", out param2descr);
		eventDescription.getParameterDescriptionByName("HeadSpeed", out param3descr);
		eventDescription.getParameterDescriptionByName("HeadRot", out param4descr);
		eventDescription.getParameterDescriptionByName("NearestPlayer", out param5descr);

		ampFollowID = param1descr.id;
		playerIndexID = param2descr.id;
		headSpeedID = param3descr.id;
		headRotID = param4descr.id;
		nearestPlayerID = param5descr.id;
		
		if(!personalEmitter.IsPlaying())
			personalEmitter.Play();
		personalEmitter.SetParameter("PlayerIndex", VRPlayerManager.trackingIndex);
		if(AudioNetworkManager.Instance != null)
        {
			personalEmitter.EventInstance.setTimelinePosition(AudioNetworkManager.Instance.masterFMODTime);
        }
		initialized = true;
	}

	private void OnDestroy()
	{
		OnPlayerNumChanged -= PlayerNumChange;
		netIdentities.Remove(ownIdentity);
		playerNum -= 1;
	}

	private void PlayerNumChange()
	{
		if (playerNum == 0)
			return;
		Array.Resize(ref distancesToPlayers, playerNum - 1);
		Debug.Log("The player list has a length of: " + netIdentities.Count);
	}

	private void Update()
    {
		if (!initialized)
			return;

		personalEmitter.SetParameter("HeadSpeed", headSpeed);
		personalEmitter.SetParameter("HeadRot", headRotation);
		if(distancesToPlayers.Length >= 1)
			personalEmitter.SetParameter("NearestPlayer", distancesToPlayers.Min());
		
    }

	/*
	float SmalestDistacneToPlayers()
    {
		float shortestDistance = Mathf.Infinity;
		for (int i = 0; i < distancesToPlayers.Length; i++)
        {
			if (distancesToPlayers[i] < shortestDistance)
				shortestDistance = distancesToPlayers[i];
		}
		return shortestDistance;
    }
	*/


    private void FixedUpdate()
	{
		if (cameraTransform != null)
		{
			headSpeed = (cameraTransform.position - prevHeadPos).magnitude / Time.fixedDeltaTime;
			headRotation = cameraTransform.rotation.eulerAngles.y;
			headRotationSpeed = (headRotation - prevHeadRot) / Time.fixedDeltaTime;
			prevHeadPos = cameraTransform.position;
			prevHeadRot = headRotation;

			int i = 0;
			foreach (NetworkIdentity netIdentity in netIdentities)
			{
				if (netIdentity == null || netIdentity == ownIdentity)
					continue;
				distancesToPlayers[i] = Vector3.Distance(cameraTransform.position, netIdentity.GetComponentInChildren<Camera>().transform.position);
				i++;
			}
		}
	}
}
