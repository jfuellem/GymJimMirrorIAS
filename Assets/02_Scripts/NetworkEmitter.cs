using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using FMODUnity;

public class NetworkEmitter : NetworkBehaviour
{
    [SyncVar]
    public int emitterTime = 0;

    public StudioEventEmitter emitter;

    private GameObject emitterObj = null;

    private void OnEnable()
    {
        if (!NetworkServer.active)
            return;


        if (!isClient)
            return;

        emitter.EventInstance.setPaused(false);
        emitter.EventInstance.setTimelinePosition(emitterTime);
    }

    private void OnDisable()
    {
        emitter.EventInstance.setPaused(true);

    }

   
    public override void OnStartClient()
    {
        base.OnStartClient();
        if(emitterObj == null)
        
       

        emitter.Play();
        emitter.EventInstance.setTimelinePosition(emitterTime);
        StartCoroutine("SyncEmitter");
    }

    private void Update()
    {
        if (!isServer)
            return;
        emitter.EventInstance.getPlaybackState(out FMOD.Studio.PLAYBACK_STATE state);
        if(state == FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            emitter.EventInstance.getTimelinePosition(out int pos);
            emitterTime = pos;
        }
        

        if (Input.GetKeyDown(KeyCode.Space))
        {
            /*
            if (play)
                emitter.EventInstance.setPaused(false);
            else
                emitter.EventInstance.setPaused(true);
            play = !play;
            */
            //emitter.EventInstance.setTimelinePosition(10000);
            //SyncEmitter();
        }
    }
    /*
    [ClientRpc]
    public void SyncEmitter()
    {
        emitter.EventInstance.setTimelinePosition(emitterTime);
    }
    */

    private IEnumerator SyncEmitter()
    {
        yield return new WaitForSeconds(5);
        emitter.EventInstance.setTimelinePosition(emitterTime);
        yield break;
    }
  
}
