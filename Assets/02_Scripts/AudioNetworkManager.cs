using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using FMODUnity;

public class AudioNetworkManager : NetworkBehaviour
{
    private FMOD.Studio.EventInstance masterInstance;

    public static AudioNetworkManager Instance { get; private set; }

    public EventReference eventRef;

    [SyncVar]
    public int masterFMODTime = 0;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        masterInstance = RuntimeManager.CreateInstance(eventRef);
        masterInstance.start();
        masterInstance.setVolume(0);
    }

    private void Update()
    {
        if (!isServer)
            return;

        masterInstance.getTimelinePosition(out masterFMODTime);
    }
}
