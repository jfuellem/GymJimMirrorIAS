using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using TMPro;
using System;

public class NetworkTransformCalc : NetworkBehaviour
{
    private float[] distancesToPlayers = new float[0];

    [SerializeField]
    static readonly List<GameObject> playerList = new();

    private int rigidBodyID;

    private OptitrackRigidBody optRb;


    public override void OnStartLocalPlayer()
    {
        SetRigidBodyID(rigidBodyID);
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        Debug.Log("Started" + gameObject);
        playerList.Add(this.gameObject);
    }


    public override void OnStopServer()
    {
        base.OnStopServer();
        playerList.Remove(this.gameObject);
    }


    private void Awake()
    {
        if (isServer)
            return;

        if (!int.TryParse(GameObject.FindGameObjectWithTag("RigidBodyIDInput").GetComponent<TMP_InputField>().text, out rigidBodyID))
            Debug.LogWarning("No rigidbody or wrong input for Player: " + netId);
    }

    [Command]
    private void SetRigidBodyID(int id)
    {
        Debug.Log(string.Format("Setting rigidbody ID: {0} for player: {1}", id, netId));
        if (optRb = gameObject.GetComponent<OptitrackRigidBody>())
        {
            optRb.enabled = true;
            optRb.RigidBodyId = id;
        }
        else
        {
            Debug.LogWarning("Optitrack rigidbody somehow not there for whatever reason...");
        }

    }

    private void FixedUpdate()
    {
        if (!isClient)
            return;

        if(distancesToPlayers.Length != playerList.Count)
            Array.Resize<float>(ref distancesToPlayers, playerList.Count - 1);

        int i = 0;
        foreach (GameObject obj in playerList)
        {
            if (obj != null && obj != gameObject)
            {
                distancesToPlayers[i] = Vector3.Distance(transform.position, obj.transform.position);
                i++;
            }
                
        }
    }

}
