using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassthroughManager : MonoBehaviour {

    public static PassthroughManager Instance;

    public OVRPassthroughLayer passthrough;
    public PassthroughPreset basePasstroughPreset;

    private void Awake()
    {
        Instance = this;

        if (this.basePasstroughPreset != null)
            this.ChangePasstrough(this.basePasstroughPreset, 0);
    }

    public void ChangePasstrough(PassthroughPreset preset, float duration)
    {
        this.StartCoroutine(this.LerpPasstrough(preset, duration));
    }

    private IEnumerator LerpPasstrough(PassthroughPreset preset, float duration)
    {

        if (this.passthrough != null)
        {
            float t = 0;

            float opacity = this.passthrough.textureOpacity;
            this.passthrough.edgeRenderingEnabled = preset.edgeRendering || this.passthrough.edgeRenderingEnabled ? true : false;

            Color edgeColor = this.passthrough.edgeColor;

            float contrast = this.passthrough.colorMapEditorContrast;
            float brighness = this.passthrough.colorMapEditorBrightness;
            float postorize = this.passthrough.colorMapEditorPosterize;
            Gradient gradient = this.passthrough.colorMapEditorGradient;

            while (t < duration)
            {
                t += Time.deltaTime;
                yield return new WaitForEndOfFrame();

                this.passthrough.textureOpacity = Mathf.Lerp(opacity,  preset.opacity, t / duration);

                this.passthrough.edgeColor = Color.Lerp(edgeColor, preset.edgeColor, t / duration);

                this.passthrough.colorMapEditorContrast = Mathf.Lerp(contrast, preset.contrast, t / duration);
                this.passthrough.colorMapEditorBrightness = Mathf.Lerp(brighness, preset.brightness, t / duration);
                this.passthrough.colorMapEditorPosterize = Mathf.Lerp(postorize, preset.posterize, t / duration);
                this.passthrough.colorMapEditorGradient = Util.Gradient.Lerp(gradient, preset.colorize, t / duration);
            }


            //after time
            this.passthrough.textureOpacity = preset.opacity;

            this.passthrough.edgeRenderingEnabled = preset.edgeRendering;
            this.passthrough.edgeColor = preset.edgeColor;

            this.passthrough.colorMapEditorContrast = preset.contrast;
            this.passthrough.colorMapEditorBrightness = preset.brightness;
            this.passthrough.colorMapEditorPosterize = preset.posterize;
            this.passthrough.colorMapEditorGradient = preset.colorize;
        }


    }
}
