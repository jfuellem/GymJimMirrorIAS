using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PasstroughPreset", menuName = "ScriptableObjects/PasstroughPreset")]
public class PassthroughPreset : ScriptableObject
{

    [Range(0, 1f)]
    public float opacity;

    public bool edgeRendering;
    public Color edgeColor;

    [Range(-1f, 1f)]
    public float contrast;
    [Range(-1f, 1f)]
    public float brightness;
    [Range(0, 1f)]
    public float posterize;

    public Gradient colorize;
}
