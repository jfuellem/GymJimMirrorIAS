using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassthroughPresetTrigger : MonoBehaviour
{
    public string tagName;
	public PassthroughPreset passthroughPreset;
	public float duration = 1f;

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag(this.tagName) && this.passthroughPreset != null && PassthroughManager.Instance != null)
		{
			PassthroughManager.Instance.ChangePasstrough(this.passthroughPreset, this.duration);
		}
	}
}
