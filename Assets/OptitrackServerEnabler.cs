using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class OptitrackServerEnabler : NetworkBehaviour
{
    public override void OnStartServer()
    {
        base.OnStartServer();
        gameObject.GetComponent<OptitrackStreamingClient>().enabled = true;
    }
}
